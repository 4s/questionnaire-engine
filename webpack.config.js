const path = require('path');
module.exports = {
    entry: './src/questionnaire-engine.ts',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      library: 'questionnaire-engine',
      libraryTarget: 'umd',
      filename: 'questionnaire-engine.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: "source-map"
};
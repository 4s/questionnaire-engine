# Questionnaire Engine
This library functions as an easy way of interacting with medical questionnaires 
from parsing through editing and serializing. The library is built on the FHIR 
specification, but is built to be generally applicable by allowing for different 
parsers and serializers

This README.md will explain the basics of installing, building and testing this library. Beyond that, the repository 
also contains a more detailed functionality and design description, as well as JSDoc documentation in the form of an 
HTML page, in the docs folder located in this library or at: 
https://bitbucket.org/4s/questionnaire-engine/src/master/docs/

## Installation
This library can be installed in two ways; using npm or manually.

### Using npm
You need npm installed on your machine (https://www.npmjs.com/get-npm)

In your project directory type the command to instal version 2.2.11.
```bash
npm install bitbucket:4s/questionnaire-engine#v2.2.11
```

### Manual installation
This repository includes both an ES6 (ES2015) version of the project and a transpiled ES5 version.
To use the ES6 version download the src-directory into your project folder and import the 
questionnaire-engine.js file into your project:
```js
import QuestionnaireEngine from './path/to/src/questionnaire-engine';

let questionnaireEngine = new QuestionnaireEngine(questionnaireParser, questionnaireSerializer);
```
If you want to use the transpiled ES5 version, just download dist/questionnaire-engine.js or 
dist/questionnaire-engine.min.js and include it in your HTML file:
```html
<script src="path/to/questionnaire-engine/dist/questionnaire-engine.js"></script>
```

## Making changes and building
This project can be used as is in either the ES6 or the ES5 version, but if changes are made to the 
sourcecode, and the ES5 version is required, the bundle files have to be rebuilt.
Webpack (https://github.com/webpack/webpack) and babel (https://babeljs.io/) are used for bundling 
and transpiling, and in order to use these frameworks, npm is required 
(https://www.npmjs.com/get-npm). 
Follow the steps below in your terminal/command prompt to build the project:

1\. Download or clone the project directory onto your machine and cd into it:
```bash
git clone https://bitbucket.org/4s/questionnaire-engine.git && cd questionnaire-engine
```
2\. Install the abovementioned dependencies:
```bash
npm install
```
(2\.a) 
```
**Make changes to the project**
```
3\. Build the project
```bash
npm run build
```
The output of the build process will be located in the dist-directory

## Versioning
To release a new version use the npm version command, for example to create a new patch version, run:
```bash
npm version patch
```
Then push with --follow-tags parameter

## Testing
Testing is performed using the Karma testrunner (https://karma-runner.github.io/2.0/index.html) and 
the Jasmine framework (https://jasmine.github.io/), Istanbul for code coverage and Webpack 
(https://github.com/webpack/webpack) and babel (https://babeljs.io/) for bundling and transpiling. 
At the time of writing, the latter two are required, as ECMAScript 6 (ECMAScript2015) is currently
not fully supported.

Therefore, to perform tests, you need to have npm installed on your machine (https://www.npmjs.com/get-npm)

To perform the tests follow the steps below in your terminal/command prompt:

1\. Download or clone the project directory onto your machine and cd into it:
```bash
git clone https://bitbucket.org/4s/questionnaire-engine.git && cd questionnaire-engine
```
2\. Install the abovementioned dependencies:
```bash
npm install
```
3\. Run the tests:
```bash
npm run test
```
The output from the test should appear directly in the terminal. A code coverage report can be found
in the root directory of the project in a folder called 'coverage'. Simply open the index.html file
in a browser to see the report.

## Support & Ownership
Any questions regarding this library should be directed at [Simon H. Madsen]
simon.madsen@alexandra.dk
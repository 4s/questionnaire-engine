import { expect } from 'chai'
import QuestionnaireResponseItem from '../src/questionnaire-response-item';
import QuestionnaireResponseItemAnswer from '../src/questionnaire-response-item-answer';
import { Observation } from 'fhir';
import { MissingPropertyException } from '../src/exceptions/exceptions';

describe('QuestionnaireResponseItem', () => {
  it('should be created', () => {
    const questionnaireResponseItem = new QuestionnaireResponseItem();
    expect(questionnaireResponseItem !== (undefined || null)).to.true;
  });

  it('should throw an error when trying to add an item that is not of the right type', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    let notQuestionnaireResponseItem = {} as any;

    expect(() => questionnaireResponseItem.addItem(notQuestionnaireResponseItem)).to.throw;
  });

  it('should throw an error when trying to replace item with item of wrong type', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    let notQuestionnaireResponseItem = {} as any;

    expect(() => questionnaireResponseItem.replaceWith(notQuestionnaireResponseItem)).to.throw;
  });

  it('should allow to replace item with item of right type', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    let otherQuestionnaireResponseItem = new QuestionnaireResponseItem();
    otherQuestionnaireResponseItem.linkId = 'item2';

    expect(questionnaireResponseItem.replaceWith(otherQuestionnaireResponseItem)).to.true;
  });

  it('should allow to set answer for item', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    questionnaireResponseItem.answer = [];

    let answer: QuestionnaireResponseItemAnswer = { valueBoolean: true, item: [] };

    expect(questionnaireResponseItem.setAnswer(answer)).to.true;
  });

  it('should allow to set answer for item with observation', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    questionnaireResponseItem.answer = [];

    let answer: QuestionnaireResponseItemAnswer = { valueBoolean: true, item: [] };

    const observation = {
      identifier: [{
        system: 'urn:ietf:rfc:3986',
        value: 'fake'
      }]
    } as Observation
    questionnaireResponseItem.setAnswer(answer, observation)

    expect(questionnaireResponseItem.answer).to.eql([{
      extension: [
        {
          url: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
          valueReference: {
            identifier: {
              system: "urn:ietf:rfc:3986",
              value: "fake"
            }
          }
        }
      ],
      item: [],
      valueBoolean: true
    }])
  });

  it('should not set observation if it doesn\'t contain UUID identifier', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    questionnaireResponseItem.answer = [];

    let answer: QuestionnaireResponseItemAnswer = { valueBoolean: true, item: [] };

    const observation = {
      identifier: [{
        system: 'not uuid system',
        value: 'fake'
      }]
    } as Observation
    
    expect(() => {questionnaireResponseItem.setAnswer(answer, observation)}).to.throw
  });

  it('should not set observation if UUID identifier doesn\'t contain value', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    questionnaireResponseItem.answer = [];

    let answer: QuestionnaireResponseItemAnswer = { valueBoolean: true, item: [] };

    const observation = {
      identifier: [{
        system: 'urn:ietf:rfc:3986',
      }]
    } as Observation
    
    expect(() => {questionnaireResponseItem.setAnswer(answer, observation)}).to.throw
  });

  it('should allow to retrieve observation reference for item with one observationreference', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    questionnaireResponseItem.answer = [{
      extension: [
        {
          url: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
          valueReference: {
            identifier: {
              system: "urn:ietf:rfc:3986",
              value: "fake"
            }
          }
        }
      ],
      item: [],
      valueBoolean: true
    }];

    expect(questionnaireResponseItem.getObservationReference()).to.eql([{
      identifier: {
        system: "urn:ietf:rfc:3986",
        value: "fake"
      }
    }])
  });

  it('should allow to retrieve observation reference array for item with two answers with observationreferences', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    questionnaireResponseItem.answer = [{
      extension: [
        {
          url: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
          valueReference: {
            identifier: {
              system: "urn:ietf:rfc:3986",
              value: "fake"
            }
          }
        }
      ],
      item: [],
      valueBoolean: true
    },
    {
      extension: [
        {
          url: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
          valueReference: {
            identifier: {
              system: "urn:ietf:rfc:3986",
              value: "fake2"
            }
          }
        }
      ],
      item: [],
      valueBoolean: true
    }]

    expect(questionnaireResponseItem.getObservationReference()).to.eql([{
      identifier: {
        system: "urn:ietf:rfc:3986",
        value: "fake"
      }
    },
    {
      identifier: {
        system: "urn:ietf:rfc:3986",
        value: "fake2"
      }
    }])
  });

    it('should allow to retrieve observation reference array for item with an answer with two observationreferences', () => {
      let questionnaireResponseItem = new QuestionnaireResponseItem();
      questionnaireResponseItem.linkId = 'item1';
      questionnaireResponseItem.answer = [{
        extension: [
          {
            url: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
            valueReference: {
              identifier: {
                system: "urn:ietf:rfc:3986",
                value: "fake"
              }
            }
          },
          {
            url: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
            valueReference: {
              identifier: {
                system: "urn:ietf:rfc:3986",
                value: "fake2"
              }
            }
          }
        ],
        item: [],
        valueBoolean: true
      }];

    expect(questionnaireResponseItem.getObservationReference()).to.eql([{
      identifier: {
        system: "urn:ietf:rfc:3986",
        value: "fake"
      }
    },
    {
      identifier: {
        system: "urn:ietf:rfc:3986",
        value: "fake2"
      }
    }])
  });

  it('should return empty array when trying to retrieve observationreference for item with no answers', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';

    expect(questionnaireResponseItem.getObservationReference()).to.eql([])
  });

  it('should return empty array when trying to retrieve observationreference for item with no extension array', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    questionnaireResponseItem.answer = [{
      item: [],
      valueBoolean: true
    }];

    expect(questionnaireResponseItem.getObservationReference()).to.eql([])
  });

  it('should return empty array when trying to retrieve observationreference for item with extension with wrong url', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item1';
    questionnaireResponseItem.answer = [{
      extension: [
        {
          url: "http://wrongurl.com",
          valueReference: {
            identifier: {
              system: "urn:ietf:rfc:3986",
              value: "fake2"
            }
          }
        }
      ],
      item: [],
      valueBoolean: true
    }];

    expect(questionnaireResponseItem.getObservationReference()).to.eql([])
  });

  it('should be able to find parent in nested strucure', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    questionnaireResponseItem.linkId = 'item';
    questionnaireResponseItem.item = [];
    let otherQuestionnaireResponseItem = new QuestionnaireResponseItem();
    otherQuestionnaireResponseItem.linkId = 'item1';
    otherQuestionnaireResponseItem.answer = [];
    let differentQuestionnaireResponseItem = new QuestionnaireResponseItem();
    differentQuestionnaireResponseItem.linkId = 'item2';
    differentQuestionnaireResponseItem.answer = [];
    let alternateQuestionnaireResponseItem = new QuestionnaireResponseItem();
    alternateQuestionnaireResponseItem.linkId = 'item3';
    alternateQuestionnaireResponseItem.answer = [];

    differentQuestionnaireResponseItem.addItem(alternateQuestionnaireResponseItem);
    otherQuestionnaireResponseItem.addItem(differentQuestionnaireResponseItem);
    questionnaireResponseItem.addItem(otherQuestionnaireResponseItem);

    let parent = questionnaireResponseItem.findParent('item3');

    expect((parent as QuestionnaireResponseItem).linkId).to.eq('item2');
  });
})


import { expect } from 'chai'
import QuestionnaireEngine, { Questionnaire, QuestionnaireResponse, QuestionnaireItem, QuestionnaireResponseItem } from '../src/questionnaire-engine';
import FhirParser from '../src/fhir-parser';
import FhirSerializer from '../src/fhir-serializer';
import jsonQuestionnaire from './resources/test-questionnaire.json';
import jsonQuestionnaireResponse from './resources/test-questionnaire-response.json';
import jsonQuestionnaireResponseNoAnswers from './resources/test-questionnaire-response-no-answers.json';
import jsonQuestionnaireFlat from './resources/flat_questionnaire.json';
import jsonQuestionnaireResponseFlatNoAnswers from './resources/flat_questionnaire_response_no_answer.json';
import jsonQuestionnaireObservationDefinition from './resources/flat_questionnaire_observationdefinition.json';
import jsonQuestionnaireEnableWhen from './resources/test-questionnaire-enable-when.json';
import fhir from 'fhir'

describe('QuestionnaireEngine', () => {
  const fhirParser = new FhirParser();
  const fhirSerializer = new FhirSerializer();

  it('should be created', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    expect(questionnaireEngine !== (undefined || null)).to.true;
  });

  it('should throw error when trying to parse questionnaire if parser is not set', () => {
    const questionnaireEngine = new QuestionnaireEngine();
    expect(() => questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire)).to.throw;
  });

  it('should parse questionnaire if parser is set', () => {
    const questionnaireEngine = new QuestionnaireEngine();
    questionnaireEngine.setParser(fhirParser);
    expect(questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire)).to.true;
  });

  it('should throw error when trying to set parser to something that doesn\' implement the correct methods', () => {
    const questionnaireEngine = new QuestionnaireEngine();
    expect(() => questionnaireEngine.setParser({} as any)).to.throw;
  });

  it('should throw error when trying to set serializer to something that doesn\' implement the correct methods', () => {
    const questionnaireEngine = new QuestionnaireEngine();
    expect(() => questionnaireEngine.setSerializer({} as any)).to.throw;
  });

  it('should throw error when trying to serialize questionnaire if serializer is not set', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);

    expect(() => questionnaireEngine.serializeQuestionnaire()).to.throw;
  });

  it('should serialize questionnaire if serializer is set', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);

    questionnaireEngine.setSerializer(fhirSerializer);
    expect(questionnaireEngine.serializeQuestionnaire()).to.exist;
  });

  it('should throw error when trying to parse questionnaire response if parser is not set', () => {
    const questionnaireEngine = new QuestionnaireEngine();

    expect(() => questionnaireEngine.parseQuestionnaireResponse(jsonQuestionnaireResponse as fhir.QuestionnaireResponse)).to.throw;
  });

  it('should parse questionnaire response if parser is set', () => {
    const questionnaireEngine = new QuestionnaireEngine();

    questionnaireEngine.setParser(fhirParser);
    expect(questionnaireEngine.parseQuestionnaireResponse(jsonQuestionnaireResponse as fhir.QuestionnaireResponse)).to.true;
  });

  it('should throw error when trying to serialize questionnaire response if serializer is not set', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser);
    questionnaireEngine.parseQuestionnaireResponse(jsonQuestionnaireResponse as fhir.QuestionnaireResponse);

    expect(() => questionnaireEngine.serializeQuestionnaireResponse()).to.throw;
  });

  it('should serialize questionnaire response if serializer is set', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser);
    questionnaireEngine.parseQuestionnaireResponse(jsonQuestionnaireResponse as fhir.QuestionnaireResponse);

    questionnaireEngine.setSerializer(fhirSerializer);
    expect(questionnaireEngine.serializeQuestionnaireResponse()).to.exist;
  });

  it('should throw error when trying to serialize questionnaire if there is no questionnaire', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    expect(() => questionnaireEngine.serializeQuestionnaire()).to.throw;
  });

  it('should throw error when trying to serialize questionnaire response if there is no questionnaire response', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    expect(() => questionnaireEngine.serializeQuestionnaireResponse()).to.throw;
  });

  it('should throw error when trying to to set questionnaire to something of a different type', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);

    let newQuestionnaireResponse = new QuestionnaireResponse();
    expect(() => questionnaireEngine.setQuestionnaire({} as any)).to.throw;
  });

  it('should throw error when trying to to return questionnaire, if there is no questionnaire', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    expect(() => questionnaireEngine.getQuestionnaire()).to.throw;
  });

  it('should return questionnaire, if there is a questionniare', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);
    expect(questionnaireEngine.getQuestionnaire()).to.exist;
  });

  it('should throw error when trying to return questionnaire response, if there is no questionnaire response', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    expect(() => questionnaireEngine.getQuestionnaireResponse()).to.throw;
  });

  it('should not throw error when trying to set questionnaire to something of type Questionnaire', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);

    let newQuestionnaire = new Questionnaire();
    expect(questionnaireEngine.setQuestionnaire(newQuestionnaire)).to.true;
  });

  it('should throw error when trying to set questionnaire response to something of a different type', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);

    let newQuestionnaireResponse = new QuestionnaireResponse();
    expect(questionnaireEngine.setQuestionnaireResponse(newQuestionnaireResponse)).to.true;
  });

  it('should create empty questionnaire response from questionnaire', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);
    let questionnaireResponse: QuestionnaireResponse = fhirParser.parseQuestionnaireResponse(jsonQuestionnaireResponseNoAnswers as fhir.QuestionnaireResponse);
    questionnaireResponse.identifier = questionnaireEngine.getQuestionnaireResponse().identifier;

    expect(questionnaireEngine.getQuestionnaireResponse()).to.eql(questionnaireResponse);
  });

  it('should be able to set answer on empty questionnaire response', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);
    questionnaireEngine.setAnswer('nestedTestItem3', 'Option 1');
    expect(questionnaireEngine.getAnswer('nestedTestItem3')).to.eq('Option 1');
  });

  it('should be able to set boolean answer to false', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaireFlat as fhir.Questionnaire);
    questionnaireEngine.setAnswer('temperatureHigh', false);
    const questionnaireResponse: fhir.QuestionnaireResponse = questionnaireEngine.serializeQuestionnaireResponse();
    questionnaireResponse.identifier = jsonQuestionnaireResponseFlatNoAnswers.identifier;

    expect(questionnaireResponse).to.eql(jsonQuestionnaireResponseFlatNoAnswers);
  });

  it('should be able to return item from questionnaire', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);
    let item = questionnaireEngine.getQuestionnaire().findItem('testItem1');
    expect(questionnaireEngine.findItem('testItem1')).to.eq(item);
  });

  it('should throw an error when trying to find item that doesn\'t exist', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);
    expect(() => questionnaireEngine.findItem('itemThatDoesntExist')).to.throw;
  });

  it('should throw an error when trying to find item when there is no questionnaire', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    expect(() => questionnaireEngine.findItem('testItem1')).to.throw;
  });

  it('should throw an error when trying to set item when there is no questionnaire', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    expect(() => questionnaireEngine.setItem('testItem1', {} as QuestionnaireItem)).to.throw;
  });

  it('should allow to set item in questionnaire and questionnaire response', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);
    let item = new QuestionnaireItem();
    item.linkId = 'testItem1';
    item.text = 'changed text';
    questionnaireEngine.setItem('testItem1', item);

    expect(questionnaireEngine.findItem('testItem1').text).to.eq('changed text');
    expect((questionnaireEngine.getQuestionnaireResponse().findItem('testItem1') as QuestionnaireResponseItem).text).to.eq('changed text');
  });

  it('should allow observationdefinition as item', () => {
    const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
    questionnaireEngine.parseQuestionnaire(jsonQuestionnaireObservationDefinition as fhir.Questionnaire);
    expect(questionnaireEngine.serializeQuestionnaire()).to.eql(jsonQuestionnaireObservationDefinition);
  });

  describe('enableWhen', () => {
    it('should throw error if item does not exist', () => {
      expect(() => {
        const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
        questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
        questionnaireEngine.isEnabled('non-existing-linkid')
      }).to.throw()
    })

    it('should return true if item does not have enableWhen', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      
      expect(
        questionnaireEngine.isEnabled('base')
      ).to.eql(true)
    })

    it('should return true when enableBehavior is any and one of the effectiveWhens is in effect', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('boolean', true)

      expect(
        questionnaireEngine.isEnabled('any')
      ).to.eql(true)
    })

    it('should return false when enableBehavior is any and none of the effectiveWhens is in effect', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('boolean', false)

      expect(
        questionnaireEngine.isEnabled('any')
      ).to.eql(false)
    })

    it('should return true when enableBehavior is all and all of the effectiveWhens is in effect', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)
      questionnaireEngine.setAnswer('boolean', true)

      expect(
        questionnaireEngine.isEnabled('all')
      ).to.eql(true)
    })

    it('should return false when enableBehavior is all and only some of the effectiveWhens is in effect', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('boolean', true)

      expect(
        questionnaireEngine.isEnabled('all')
      ).to.eql(false)
    })

    it('should return false when enableBehavior is all and none of the effectiveWhens is in effect', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('all', false)

      expect(
        questionnaireEngine.isEnabled('integer')
      ).to.eql(false)
    })

    it('should assume \'any\' when no enableBehavior is defined', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('not-defined')
      ).to.eql(true)
    })

    it('should be able to tell that boolean questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('boolean')
      ).to.eql(true)
    })

    it('should be able to tell that boolean questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('boolean')
      ).to.eql(false)
    })

    it('should be able to tell that decimal questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('decimal')
      ).to.eql(true)
    })

    it('should be able to tell that decimal questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('decimal')
      ).to.eql(false)
    })

    it('should be able to tell that integer questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('integer')
      ).to.eql(true)
    })

    it('should be able to tell that integer questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('integer')
      ).to.eql(false)
    })

    it('should be able to tell that date questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('date')
      ).to.eql(true)
    })

    it('should be able to tell that date questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('date')
      ).to.eql(false)
    })

    it('should be able to tell that dateTime questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('dateTime')
      ).to.eql(true)
    })

    it('should be able to tell that dateTime questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('dateTime')
      ).to.eql(false)
    })

    it('should be able to tell that time questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('time')
      ).to.eql(true)
    })

    it('should be able to tell that time questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('time')
      ).to.eql(false)
    })

    it('should be able to tell that string questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('string')
      ).to.eql(true)
    })

    it('should be able to tell that string questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('string')
      ).to.eql(false)
    })

    it('should be able to tell that uri questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('uri')
      ).to.eql(true)
    })

    it('should be able to tell that uri questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('uri')
      ).to.eql(false)
    })

    it('should be able to tell that Coding questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('Coding')
      ).to.eql(true)
    })

    it('should be able to tell that Coding questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('Coding')
      ).to.eql(false)
    })

    it('should be able to tell that Quantity questions are enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 1)

      expect(
        questionnaireEngine.isEnabled('Quantity')
      ).to.eql(true)
    })

    it('should be able to tell that Quantity questions are not enabled', () => {
      const questionnaireEngine = new QuestionnaireEngine(fhirParser, fhirSerializer);
      questionnaireEngine.parseQuestionnaire(jsonQuestionnaireEnableWhen as fhir.Questionnaire)
      questionnaireEngine.setAnswer('base', 2)

      expect(
        questionnaireEngine.isEnabled('Quantity')
      ).to.eql(false)
    })
  })
  
});

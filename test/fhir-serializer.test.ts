import { expect } from 'chai'
import { Questionnaire, QuestionnaireItem, QuestionnaireResponse, QuestionnaireResponseItem } from '../src/questionnaire-engine';
import FhirSerializer from '../src/fhir-serializer';

// resources
import jsonQuestionnaire from './resources/test-questionnaire.json'
import jsonQuestionnaireResponse from './resources/test-questionnaire-response.json'

describe('FhirSerializer', () => {
  it('should serialize questionnaire correctly', () => {
    let fhirSerializer = new FhirSerializer();

    let questionnaire = new Questionnaire();
    questionnaire.name = 'test questionnaire';
    questionnaire.title = 'test questionnaire';
    questionnaire.status = 'active'
    questionnaire.description = 'A test questionnaire';
    questionnaire.subjectType = ['Patient'];

    let questionnaireItemOne = new QuestionnaireItem();
    questionnaireItemOne.linkId = 'testItem1';
    questionnaireItemOne.type = 'group';
    questionnaireItemOne.text = 'test item no. 1';

    let nestedQuestionnaireItemOne = new QuestionnaireItem();
    nestedQuestionnaireItemOne.linkId = 'nestedTestItem1';
    nestedQuestionnaireItemOne.type = 'boolean';
    nestedQuestionnaireItemOne.text = 'nested test item no. 1';
    nestedQuestionnaireItemOne.definition = 'test definition';

    let nestedQuestionnaireItemOneOne = new QuestionnaireItem();
    nestedQuestionnaireItemOneOne.linkId = 'nestedTestItem1.1';
    nestedQuestionnaireItemOneOne.type = 'string';
    nestedQuestionnaireItemOneOne.text = 'nested test item no. 1.1';

    nestedQuestionnaireItemOne.addItem(nestedQuestionnaireItemOneOne);

    let nestedQuestionnaireItemTwo = new QuestionnaireItem();
    nestedQuestionnaireItemTwo.linkId = 'nestedTestItem2';
    nestedQuestionnaireItemTwo.type = 'string';
    nestedQuestionnaireItemTwo.text = 'nested test item no. 2';

    let nestedQuestionnaireItemThree = new QuestionnaireItem();
    nestedQuestionnaireItemThree.linkId = 'nestedTestItem3';
    nestedQuestionnaireItemThree.type = 'choice';
    nestedQuestionnaireItemThree.text = 'nested test item no. 3';
    nestedQuestionnaireItemThree.answerOption = [];
    nestedQuestionnaireItemThree.answerOption.push({
      valueCoding: {
        code: 'Option 1'
      }
    });
    nestedQuestionnaireItemThree.answerOption.push({
      valueCoding: {
        code: 'Option 2'
      }
    });

    questionnaireItemOne.addItem(nestedQuestionnaireItemOne);
    questionnaireItemOne.addItem(nestedQuestionnaireItemTwo);
    questionnaireItemOne.addItem(nestedQuestionnaireItemThree);

    let questionnaireItemTwo = new QuestionnaireItem();
    questionnaireItemTwo.linkId = 'testItem2';
    questionnaireItemTwo.type = 'decimal';
    questionnaireItemTwo.text = 'test item no. 2';

    questionnaire.addItem(questionnaireItemOne);
    questionnaire.addItem(questionnaireItemTwo);

    expect(fhirSerializer.serializeQuestionnaire(questionnaire)).to.eql(jsonQuestionnaire);
  });

  it('should serialize questionnaire response correctly', () => {
    let fhirSerializer = new FhirSerializer();

    let questionnaireResponse = new QuestionnaireResponse();
    questionnaireResponse.status = 'completed';

    let questionnaireResponseItemOne = new QuestionnaireResponseItem();
    questionnaireResponseItemOne.linkId = 'testItem1';
    questionnaireResponseItemOne.text = 'test item no. 1';
    questionnaireResponseItemOne.item = [];

    let nestedQuestionnaireResponseItemOneOne = new QuestionnaireResponseItem();
    nestedQuestionnaireResponseItemOneOne.linkId = 'nestedTestItem1.1';
    nestedQuestionnaireResponseItemOneOne.text = 'nested test item no. 1.1';
    nestedQuestionnaireResponseItemOneOne.answer = [];
    nestedQuestionnaireResponseItemOneOne.answer.push({
      valueString: 'answer'
    });

    let nestedQuestionnaireResponseItemOne = new QuestionnaireResponseItem();
    nestedQuestionnaireResponseItemOne.linkId = 'nestedTestItem1';
    nestedQuestionnaireResponseItemOne.text = 'nested test item no. 1';
    nestedQuestionnaireResponseItemOne.definition = 'test definition';
    nestedQuestionnaireResponseItemOne.answer = [];
    nestedQuestionnaireResponseItemOne.answer.push({
      valueBoolean: true,
      item: []
    });

    nestedQuestionnaireResponseItemOne.addItem(nestedQuestionnaireResponseItemOneOne);

    let nestedQuestionnaireResponseItemTwo = new QuestionnaireResponseItem();
    nestedQuestionnaireResponseItemTwo.linkId = 'nestedTestItem2';
    nestedQuestionnaireResponseItemTwo.text = 'nested test item no. 2';
    nestedQuestionnaireResponseItemTwo.answer = [];
    nestedQuestionnaireResponseItemTwo.answer.push({
      valueString: 'A test answer'
    });

    let nestedQuestionnaireResponseItemThree = new QuestionnaireResponseItem();
    nestedQuestionnaireResponseItemThree.linkId = 'nestedTestItem3';
    nestedQuestionnaireResponseItemThree.text = 'nested test item no. 3';
    nestedQuestionnaireResponseItemThree.answer = [];
    nestedQuestionnaireResponseItemThree.answer.push({
      valueCoding: {
        code: 'Option 1'
      }
    });

    questionnaireResponseItemOne.addItem(nestedQuestionnaireResponseItemOne);
    questionnaireResponseItemOne.addItem(nestedQuestionnaireResponseItemTwo);
    questionnaireResponseItemOne.addItem(nestedQuestionnaireResponseItemThree);

    let questionnaireResponseItemTwo = new QuestionnaireResponseItem();
    questionnaireResponseItemTwo.linkId = 'testItem2';
    questionnaireResponseItemTwo.text = 'test item no. 2';
    questionnaireResponseItemTwo.answer = [];
    questionnaireResponseItemTwo.answer.push({
      valueDecimal: 3.14
    });

    questionnaireResponse.addItem(questionnaireResponseItemOne);
    questionnaireResponse.addItem(questionnaireResponseItemTwo);

    expect(
      JSON.parse(JSON.stringify(fhirSerializer.serializeQuestionnaireResponse(questionnaireResponse)))
    ).to.deep.equal(
      JSON.parse(JSON.stringify(jsonQuestionnaireResponse))
    );
  });
});

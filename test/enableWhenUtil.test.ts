import { expect } from 'chai'
import { enableWhenUtil } from '../src/helpers/enableWhenUtil';

describe('enableWhenUtil', () => {
  describe('stringToOperator', () => {
    it('should return function that always returns true if no operator is provided', () => {
      expect(enableWhenUtil.stringToOperator()('string', 'string', 'string')).to.eql(true)
      expect(enableWhenUtil.stringToOperator()('integer', '7', '10')).to.eql(true)
      expect(enableWhenUtil.stringToOperator()('date', '1951-06-04', '1922-02-03')).to.eql(true)
    })

    it('should return true when testing if an input that exists, exists', () => {
      expect(enableWhenUtil.stringToOperator('exists')('string', 'string')).to.eql(true)
    })

    it('should return false when testing if an input that does not exist, exists', () => {
      expect(enableWhenUtil.stringToOperator('exists')('string', undefined)).to.eql(false)
    })

    it('should return true when testing if two inputs that are equal, are equal', () => {
      expect(enableWhenUtil.stringToOperator('=')('string', 'string', 'string')).to.eql(true)
    })

    it('should return false when testing if two inputs that are not equal, are equal', () => {
      expect(enableWhenUtil.stringToOperator('=')('string', 'string', 'otherString')).to.eql(false)
    })

    it('should return true when testing if two inputs that are not equal, are not equal', () => {
      expect(enableWhenUtil.stringToOperator('!=')('string', 'string', 'otherString')).to.eql(true)
    })

    it('should return false when testing if two inputs that are equal, are not equal', () => {
      expect(enableWhenUtil.stringToOperator('!=')('string', 'string', 'string')).to.eql(false)
    })

    it('should return true when testing if an input that is larger than another input, is larger than another input', () => {
      expect(enableWhenUtil.stringToOperator('>')('integer', 3, 2)).to.eql(true)
    })

    it('should return false when testing if an input that is smaller than another input, is larger than another input', () => {
      expect(enableWhenUtil.stringToOperator('>')('integer', 2, 3)).to.eql(false)
    })

    it('should return false when testing if an input that is equal to another input, is larger than another input', () => {
      expect(enableWhenUtil.stringToOperator('>')('integer', 3, 3)).to.eql(false)
    })

    it('should return true when testing if an input that is smaller than another input, is smaller than another input', () => {
      expect(enableWhenUtil.stringToOperator('<')('integer', 2, 3)).to.eql(true)
    })

    it('should return false when testing if an input that is larger than another input, is smaller than another input', () => {
      expect(enableWhenUtil.stringToOperator('<')('integer', 3, 2)).to.eql(false)
    })

    it('should return false when testing if an input that is equal to another input, is smaller than another input', () => {
      expect(enableWhenUtil.stringToOperator('<')('integer', 3, 3)).to.eql(false)
    })
    
    it('should return true when testing if an input that is larger than another input, is larger than or equal to another input', () => {
      expect(enableWhenUtil.stringToOperator('>=')('integer', 3, 2)).to.eql(true)
    })

    it('should return false when testing if an input that is smaller than another input, is larger than or equal to another input', () => {
      expect(enableWhenUtil.stringToOperator('>=')('integer', 2, 3)).to.eql(false)
    })

    it('should return true when testing if an input that is equal to another input, is larger than or equal to another input', () => {
      expect(enableWhenUtil.stringToOperator('>=')('integer', 3, 3)).to.eql(true)
    })

    it('should return true when testing if an input that is smaller than another input, is smaller than or equal to another input', () => {
      expect(enableWhenUtil.stringToOperator('<=')('integer', 2, 3)).to.eql(true)
    })

    it('should return false when testing if an input that is larger than another input, is smaller than or equal to another input', () => {
      expect(enableWhenUtil.stringToOperator('<=')('integer', 3, 2)).to.eql(false)
    })

    it('should return true when testing if an input that is equal to another input, is smaller than or equal to another input', () => {
      expect(enableWhenUtil.stringToOperator('<=')('integer', 3, 3)).to.eql(true)
    })
  })

  describe('compare', () => {
    it('should be able to calculate that two booleans are equal', () => {
      expect(enableWhenUtil.compare('boolean', true, true)).to.eql(0)
    })

    it('should be able to calculate that two booleans are not equal', () => {
      expect(enableWhenUtil.compare('boolean', true, false)).to.not.eql(0)
    })

    it('should be able to calculate that two numbers are equal', () => {
      expect(enableWhenUtil.compare('decimal', 1, 1)).to.eql(0)
    })

    it('should be able to calculate that a number is higher than another number', () => {
      expect(enableWhenUtil.compare('decimal', 2, 1)).to.be.above(0)
    })

    it('should be able to calculate that a number is lower than another number', () => {
      expect(enableWhenUtil.compare('decimal', 1, 2)).to.be.below(0)
    })

    it('should be able to calculate that two dates are equal', () => {
      expect(enableWhenUtil.compare('date', '1951-06-04', '1951-06-04')).to.eql(0)
    })

    it('should be able to calculate that a date is larger than another date', () => {
      expect(enableWhenUtil.compare('date', '1952-06-04', '1951-06-04')).to.be.above(0)
    })

    it('should be able to calculate that a date is smaller than another date', () => {
      expect(enableWhenUtil.compare('date', '1951-06-04', '1952-06-04')).to.be.below(0)
    })

    it('should be able to calculate that two dateTimes are equal', () => {
      expect(enableWhenUtil.compare('dateTime', '2013-06-08T10:57:34+01:00', '2013-06-08T10:57:34+01:00')).to.eql(0)
    })

    it('should be able to calculate that a dateTime is larger than another dateTime', () => {
      expect(enableWhenUtil.compare('dateTime', '2014-06-08T10:57:34+01:00', '2013-06-08T10:57:34+01:00')).to.be.above(0)
    })

    it('should be able to calculate that a dateTime is smaller than another dateTime', () => {
      expect(enableWhenUtil.compare('dateTime', '2013-06-08T10:57:34+01:00', '2014-06-08T10:57:34+01:00')).to.be.below(0)
    })

    it('should be able to calculate that two times are equal', () => {
      expect(enableWhenUtil.compare('time', '14:35', '14:35')).to.eql(0)
    })

    it('should be able to calculate that a time is larger than another time', () => {
      expect(enableWhenUtil.compare('time', '15:35', '14:35')).to.be.above(0)
    })

    it('should be able to calculate that a time is smaller than another time', () => {
      expect(enableWhenUtil.compare('time', '14:35', '15:35')).to.be.below(0)
    })

    it('should be able to calculate that two strings are lexicographically equal', () => {
      expect(enableWhenUtil.compare('string', 'aaa', 'aaa')).to.eql(0)
    })

    it('should be able to calculate that a string is lexicographically larger than another string', () => {
      expect(enableWhenUtil.compare('string', 'bbb', 'aaa')).to.be.above(0)
    })

    it('should be able to calculate that a string is lexicographically smaller than another string', () => {
      expect(enableWhenUtil.compare('string', 'aaa', 'bbb')).to.be.below(0)
    })

    it('should be able to calculate that two Codings are equal', () => {
      expect(enableWhenUtil.compare('Coding', {
        system: 'system',
        code: 'code'
      }, {
        system: 'system',
        code: 'code'
      })).to.eql(0)
    })

    it('should be able to calculate that two Codings are not equal when systems are the same but codings are different', () => {
      expect(enableWhenUtil.compare('Coding', {
        system: 'system',
        code: 'code'
      }, {
        system: 'system',
        code: 'anotherCode'
      })).to.not.eql(0)
    })

    it('should be able to calculate that two Codings are not equal when systems are different', () => {
      expect(enableWhenUtil.compare('Coding', {
        system: 'anotherSystem',
        code: 'code'
      }, {
        system: 'system',
        code: 'code'
      })).to.not.eql(0)
    })

    it('should be able to calculate that two Quantities in the same system and with the same code are equal', () => {
      expect(enableWhenUtil.compare('Quantity', {
        system: 'system',
        code: 'code',
        value: 2.5
      }, {
        system: 'system',
        code: 'code',
        value: 2.5
      })).to.eql(0)
    })

    it('should be able to calculate that a Quantity is larger than another Quantity within the same system and with the same code ', () => {
      expect(enableWhenUtil.compare('Quantity', {
        system: 'system',
        code: 'code',
        value: 2.6
      }, {
        system: 'system',
        code: 'code',
        value: 2.5
      })).to.be.above(0)
    })

    it('should be able to calculate that a Quantity is smaller than another Quantity within the same system and with the same code ', () => {
      expect(enableWhenUtil.compare('Quantity', {
        system: 'system',
        code: 'code',
        value: 2.5
      }, {
        system: 'system',
        code: 'code',
        value: 2.6
      })).to.be.below(0)
    })

    it('should be able to calculate that two Quantities with the same unit are equal', () => {
      expect(enableWhenUtil.compare('Quantity', {
        unit: 'kg',
        value: 2.5
      }, {
        unit: 'kg',
        value: 2.5
      })).to.eql(0)
    })

    it('should be able to calculate that a Quantity is larger than another Quantity with the same unit', () => {
      expect(enableWhenUtil.compare('Quantity', {
        unit: 'kg',
        value: 2.6
      }, {
        unit: 'kg',
        value: 2.5
      })).to.be.above(0)
    })

    it('should be able to calculate that a Quantity is smaller than another Quantity with the same unit', () => {
      expect(enableWhenUtil.compare('Quantity', {
        unit: 'kg',
        value: 2.5
      }, {
        unit: 'kg',
        value: 2.6
      })).to.be.below(0)
    })

    it('should return undefined when trying to compare Quantities without both unit and system/code',() => {
      expect(enableWhenUtil.compare('Quantity', {
        value: 2.5
      }, {
        value: 2.6
      })).to.be.an('undefined')
    })
  })
  
})
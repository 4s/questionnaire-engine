import { expect } from 'chai'
import QuestionnaireItem from '../src/questionnaire-item';

describe('QuestionnaireItem', () => {
  it('should be created', () => {
    const questionnaireItem = new QuestionnaireItem();
    expect(questionnaireItem !== (undefined || null)).to.true;
  });

  it('should throw an error when trying to add an item that is not of the right type', () => {
    let questionnaireItem = new QuestionnaireItem();
    let notQuestionnaireItem = {} as any;

    expect(() => questionnaireItem.addItem(notQuestionnaireItem)).to.throw;
  });

  it('should throw an error when trying to replace item with item of wrong type', () => {
    let questionnaireItem = new QuestionnaireItem();
    let notQuestionnaireItem = {} as any;

    expect(() => questionnaireItem.replaceWith(notQuestionnaireItem)).to.throw;
  });

  it('should allow to replace item with item of right type', () => {
    let questionnaireItem = new QuestionnaireItem();
    questionnaireItem.linkId = 'item1';
    let otherQuestionnaireItem = new QuestionnaireItem();
    otherQuestionnaireItem.linkId = 'item2';

    expect(questionnaireItem.replaceWith(otherQuestionnaireItem)).to.true;
  });

  it('should be able to iterate over items', () => {
    let questionnaireItem = new QuestionnaireItem();
    questionnaireItem.linkId = 'item';
    let otherQuestionnaireItem = new QuestionnaireItem();
    otherQuestionnaireItem.linkId = 'item1';
    let differentQuestionnaireItem = new QuestionnaireItem();
    differentQuestionnaireItem.linkId = 'item2';
    let alternateQuestionnaireItem = new QuestionnaireItem();
    alternateQuestionnaireItem.linkId = 'item3';

    otherQuestionnaireItem.addItem(alternateQuestionnaireItem);
    questionnaireItem.addItem(otherQuestionnaireItem);
    questionnaireItem.addItem(differentQuestionnaireItem);

    let count = 0;
    for (let item of questionnaireItem) {
      if (item && item instanceof QuestionnaireItem) {
        count++;
      }
    }
    expect(count).to.eq(3);
  });

  it('should allow for retrieving DeviceDefinitionId', () => {
    let topQuestionnaireItem = new QuestionnaireItem();
    topQuestionnaireItem.linkId = 'topQuestionnaireItem';
    topQuestionnaireItem.type = 'group';
    let deviceQuestionnaireItem = new QuestionnaireItem();
    deviceQuestionnaireItem.linkId = 'groupItem';
    deviceQuestionnaireItem.type = 'group';
    deviceQuestionnaireItem.extension = [{
      url: 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/114720777/Questionnaire+optionalMeasurement',
      valueReference: {
        reference: 'devdef1', // Weight scale
        type: 'DeviceDefinition'
      }
    }];
    let questionnaireItem = new QuestionnaireItem();
    questionnaireItem.linkId = 'item';
    questionnaireItem.type = 'quantity';
    questionnaireItem.extension = [{
      url: 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115539981/Questionnaire+observationDefinition',
      valueReference: {
        reference: 'obsdef1', // Weight scale
        type: 'ObservationDefinition'
      }
    }];
    deviceQuestionnaireItem.addItem(questionnaireItem);
    let otherQuestionnaireItem = new QuestionnaireItem();
    otherQuestionnaireItem.linkId = 'otherItem';
    otherQuestionnaireItem.type = 'integer';
    topQuestionnaireItem.addItem(deviceQuestionnaireItem);
    topQuestionnaireItem.addItem(otherQuestionnaireItem);
    for (let item of topQuestionnaireItem) {
      if (item.getType() === 'device') {
        expect(item.getDeviceDefinitionReference()).to.eql({ reference: 'devdef1', type: 'DeviceDefinition' });
      } else {
        expect(() => item.getDeviceDefinitionReference()).to.throw;
      }
    }
  });
});

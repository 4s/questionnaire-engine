import { expect } from 'chai'
import { QuestionnaireResponseItem, QuestionnaireResponse } from '../src/questionnaire-engine';
import FhirParser from '../src/fhir-parser';
import jsonQuestionnaireResponse from './resources/test-questionnaire-response.json';
import editedJsonQuestionnaireResponse from './resources/test-questionnaire-response-edited.json';
import fhir = require('fhir');

describe('QuestionnaireResponse', () => {
  let questionnaireResponse: QuestionnaireResponse;

  beforeEach(() => {
    questionnaireResponse = new FhirParser().parseQuestionnaireResponse(jsonQuestionnaireResponse as fhir.QuestionnaireResponse);
  });

  it('should find nested items', () => {
    let item: QuestionnaireResponseItem = (questionnaireResponse.findItem('nestedTestItem2') as  QuestionnaireResponseItem);
    expect(item.text).to.eq('nested test item no. 2');
  });

  it('should be able to change item', () => {
    let editedQuestionnaireResponse = new FhirParser().parseQuestionnaireResponse(editedJsonQuestionnaireResponse as fhir.QuestionnaireResponse);

    let newText = 'changed nested test item no. 3';
    (questionnaireResponse.findItem('nestedTestItem3') as QuestionnaireResponseItem).text = newText;
    expect(questionnaireResponse).to.eql(editedQuestionnaireResponse);
  });

  it('should be able to replace item', () => {
    let questionnaireResponseItem = new QuestionnaireResponseItem();
    expect(questionnaireResponse.findItem('nestedTestItem3')).to.exist;
    questionnaireResponse.setItem('nestedTestItem3', questionnaireResponseItem);
    expect(questionnaireResponse.findItem('nestedTestItem3')).to.not.exist;
  });

  it('should be able to delete an item', () => {
    let questionnaireItem = new QuestionnaireResponseItem();
    Object.assign(questionnaireItem, questionnaireResponse.findItem('testItem2'));
    expect(questionnaireResponse.findItem('testItem2')).to.exist;
    questionnaireResponse.deleteItem('testItem2');
    expect(questionnaireResponse.findItem('testItem2')).to.not.exist;
  });

  it('should be able to add item', () => {
    let questionnaireItem = new QuestionnaireResponseItem();
    questionnaireItem.linkId = 'addedTestItem';
    questionnaireResponse.addItem(questionnaireItem);
    expect(questionnaireResponse.findItem('addedTestItem')).to.exist;
  });

  it('should be able to iterate over items', () => {
    let count = 0;
    for (let item of questionnaireResponse) {
      if (item) {
        count++;
      }
    }
    expect(count).to.eq(6);
  });
});

import { expect } from 'chai'
import { QuestionnaireItem, Questionnaire } from '../src/questionnaire-engine';
import FhirParser from '../src/fhir-parser';
import jsonQuestionnaire from './resources/test-questionnaire.json';
import editedJsonQuestionnaire from './resources/test-questionnaire-edited.json';
import deviceAndObservationQuestionnaire from './resources/test-questionnaire-with-device-and-observation.json';
import mixedDeviceAndObservationQuestionnaire from './resources/test-questionnaire-with-observations.json';
import { Reference } from 'fhir';
import fhir = require('fhir');


describe('Questionnaire', () => {
  let questionnaire: Questionnaire;
  let testQuestionnaire: Questionnaire;
  let editedTestQuestionnaire: Questionnaire;
  let deviceAndObservationTestQuestionnaire: Questionnaire | null
  let mixedDeviceAndObservationTestQuestionnaire: Questionnaire | null

  beforeEach(() => {
    questionnaire = new FhirParser().parseQuestionnaire(jsonQuestionnaire as fhir.Questionnaire);
    mixedDeviceAndObservationTestQuestionnaire = {} as Questionnaire
    Object.assign(mixedDeviceAndObservationTestQuestionnaire, mixedDeviceAndObservationQuestionnaire)
    deviceAndObservationTestQuestionnaire = {} as Questionnaire
    Object.assign(deviceAndObservationTestQuestionnaire, deviceAndObservationQuestionnaire)
  });

  afterEach(() => {
    deviceAndObservationTestQuestionnaire = null
  });

  it('should find nested items', () => {
    let item = questionnaire.findItem('nestedTestItem2');
    expect((item as QuestionnaireItem).text).to.eq('nested test item no. 2');
  });

  it('should be able to change item', () => {
    let editedQuestionnaire = new FhirParser().parseQuestionnaire(editedJsonQuestionnaire as fhir.Questionnaire);

    let newText = 'changed nested test item no. 2';
    (questionnaire.findItem('nestedTestItem2') as QuestionnaireItem).text = newText;
    expect(questionnaire).to.eql(editedQuestionnaire);
  });

  it('should be able to replace item', () => {
    let questionnaireItem = new QuestionnaireItem();
    expect(questionnaire.setItem('nestedTestItem3', questionnaireItem)).to.true;
  });

  it('should throw error when trying to replace item that doesn\'t exist', () => {
    let questionnaireItem = new QuestionnaireItem();
    expect(() => questionnaire.setItem('itemThatDoesntExist', questionnaireItem)).to.throw;
  });

  it('should be able to delete an item', () => {
    expect(questionnaire.deleteItem('testItem2')).to.true;
  });

  it('should throw error when trying to delete an item that doesn\'t exist', () => {
    expect(() => questionnaire.deleteItem('itemThatDoesntExist')).to.throw;
  });

  it('should be able to add item', () => {
    let questionnaireItem = new QuestionnaireItem();
    questionnaireItem.linkId = 'addedTestItem';
    questionnaire.addItem(questionnaireItem);
    expect(questionnaire.findItem('addedTestItem')).to.exist;
  });

  it('should throw error when trying to add item with no linkId', () => {
    let questionnaireItem = new QuestionnaireItem();

    expect(() => questionnaire.addItem(questionnaireItem)).to.throw;
  });

  it('should throw error when trying to add item to a parentItem that doesnt exist', () => {
    let questionnaireItem = new QuestionnaireItem();
    questionnaireItem.linkId = 'addedTestItem';

    expect(() => questionnaire.addItem(questionnaireItem, 'parentItemThatDoesntExist')).to.throw;
  });

  it('should throw error when trying to add item that already exists', () => {
    let questionnaireItem = new QuestionnaireItem();
    questionnaireItem.linkId = 'addedTestItem';
    questionnaire.addItem(questionnaireItem);

    expect(() => questionnaire.addItem(questionnaireItem)).to.throw;
  });

  it('should be able to add item to another item in the nested structure', () => {
    let questionnaireItem = new QuestionnaireItem();
    questionnaireItem.linkId = 'item';
    let otherQuestionnaireItem = new QuestionnaireItem();
    otherQuestionnaireItem.linkId = 'item1';
    let differentQuestionnaireItem = new QuestionnaireItem();
    differentQuestionnaireItem.linkId = 'item2';
    let alternateQuestionnaireItem = new QuestionnaireItem();
    alternateQuestionnaireItem.linkId = 'item3';

    otherQuestionnaireItem.addItem(alternateQuestionnaireItem);
    questionnaireItem.addItem(otherQuestionnaireItem);
    questionnaire.addItem(questionnaireItem);

    expect(questionnaire.addItem(differentQuestionnaireItem, alternateQuestionnaireItem.linkId)).to.true;
  });

  it('should be able to iterate over items', () => {
    let count = 0;
    for (let item of questionnaire) {
      if (item) {
        count++;
      }
    }
    expect(count).to.eq(6);
  });


  it('should be able to tell, if an item is of type "observation-group"', () => {
    let questionnaire = new FhirParser().parseQuestionnaire((mixedDeviceAndObservationTestQuestionnaire as Questionnaire));
    for (let item of questionnaire) {
      if (item.linkId === 'group4') {
        expect(item.getType()).to.eql('observation-group')
      }
    }
  })

  it('should be able to find DeviceDefinitionReference for QuestionnaireItem of type observation', () => {
    let observationQuestionnaire = new FhirParser().parseQuestionnaire((deviceAndObservationTestQuestionnaire as Questionnaire));
    let deviceDefinitionReference = observationQuestionnaire.getDeviceDefinitionReferenceForItem('observation1');
    expect(deviceDefinitionReference).to.eql({ reference: 'devdef1', type: 'DeviceDefinition' });
  });

  it('should be able to find DeviceDefinition for QuestionnaireItem of type observation', () => {
    let observationQuestionnaire = new FhirParser().parseQuestionnaire((deviceAndObservationTestQuestionnaire as Questionnaire))
    let deviceDefinitionReference = observationQuestionnaire.getDeviceDefinitionReferenceForItem('observation1');
    let deviceDefinition = observationQuestionnaire.getContainedResourceById(((deviceDefinitionReference as Reference).reference as string));
    expect(deviceDefinition).to.eql({
      resourceType: 'DeviceDefinition',
      id: 'devdef1',
      type: {
        system: 'urn:iso:std:iso:11073:10101',
        code: '528399',
        display: 'MDC_DEV_SPEC_PROFILE_SCALE'
      }
    });
  });
});

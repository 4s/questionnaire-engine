import { expect } from 'chai'
import QuestionnaireEngine from '../src/questionnaire-engine';
import FhirParser from '../src/fhir-parser';
import FhirSerializer from '../src/fhir-serializer';
import bloodPressureQuestionnaire from './resources/bloodpressure-questionnaire.json';
import bloodSugarQuestionnaire from './resources/bloodsugar-questionnaire.json';
import crpQuestionnaire from './resources/CRP-questionnaire.json';
import diabetesQuestionnaire from './resources/diabetes-questionnaire.json'
import ppromQuestionnaire from './resources/PPROM-questionnaire.json'
import preeclampsiaQuestionnaire from './resources/preeclampsia-questionnaire.json'
import fhir = require('fhir');

describe('OpenTele Questionnaires', () => {
  let questionnaireEngine: QuestionnaireEngine;

  beforeEach(() => {
    questionnaireEngine = new QuestionnaireEngine(new FhirParser(), new FhirSerializer());
  });

  it('should be able to parse bloodpressure-questionnaire', () => {
    expect(questionnaireEngine.parseQuestionnaire(bloodPressureQuestionnaire as fhir.Questionnaire)).to.true;
  });

  it('should be able to serialize bloodpressure-questionnaire', () => {
    questionnaireEngine.parseQuestionnaire(bloodPressureQuestionnaire as fhir.Questionnaire);
    expect(questionnaireEngine.serializeQuestionnaire()).to.eql(bloodPressureQuestionnaire);
  });

  it('should be able to create questionnaire response for bloodpressure questionnaire', () => {
    questionnaireEngine.parseQuestionnaire(bloodPressureQuestionnaire as fhir.Questionnaire);
    expect(questionnaireEngine.serializeQuestionnaireResponse()).to.exist;
  });

  it('should be able to parse bloodsugar-questionnaire', () => {
    expect(questionnaireEngine.parseQuestionnaire(bloodSugarQuestionnaire as fhir.Questionnaire)).to.true;
  });

  it('should be able to serialize bloodsugar-questionnaire', () => {
    questionnaireEngine.parseQuestionnaire(bloodSugarQuestionnaire as fhir.Questionnaire);
    expect(questionnaireEngine.serializeQuestionnaire()).to.eql(bloodSugarQuestionnaire);
  });

  it('should be able to parse CRP-questionnaire', () => {
    expect(questionnaireEngine.parseQuestionnaire(crpQuestionnaire as fhir.Questionnaire)).to.true;
  });

  it('should be able to serialize CRP-questionnaire', () => {
    questionnaireEngine.parseQuestionnaire(crpQuestionnaire as fhir.Questionnaire);
    expect(questionnaireEngine.serializeQuestionnaire()).to.eql(crpQuestionnaire);
  });

  it('should be able to parse diabetes-questionnaire', () => {
    expect(questionnaireEngine.parseQuestionnaire(diabetesQuestionnaire as fhir.Questionnaire)).to.true;
  });

  it('should be able to serialize diabetes-questionnaire', () => {
    questionnaireEngine.parseQuestionnaire(diabetesQuestionnaire as fhir.Questionnaire);
    expect(questionnaireEngine.serializeQuestionnaire()).to.eql(diabetesQuestionnaire);
  });

  it('should be able to parse PPROM-questionnaire', () => {
    expect(questionnaireEngine.parseQuestionnaire(ppromQuestionnaire as fhir.Questionnaire)).to.true;
  });

  it('should be able to serialize PPROM-questionnaire', () => {
    questionnaireEngine.parseQuestionnaire(ppromQuestionnaire as fhir.Questionnaire);
    expect(questionnaireEngine.serializeQuestionnaire()).to.eql(ppromQuestionnaire);
  });

  it('should be able to parse preeclampsia-questionnaire', () => {
    expect(questionnaireEngine.parseQuestionnaire(preeclampsiaQuestionnaire as fhir.Questionnaire)).to.true;
  });

  it('should be able to serialize preeclampsia-questionnaire', () => {
    questionnaireEngine.parseQuestionnaire(preeclampsiaQuestionnaire as fhir.Questionnaire);
    expect(questionnaireEngine.serializeQuestionnaire()).to.eql(preeclampsiaQuestionnaire);
  });
});

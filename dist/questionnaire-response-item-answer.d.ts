import * as fhir from 'fhir';
import { QuestionnaireResponseItem } from './questionnaire-engine';
/**
 * Class representing an Item for Questionnaire Responses.
 */
export default class QuestionnaireResponseItemAnswer implements fhir.QuestionnaireResponseItemAnswer {
    /**
     * Additional Content defined by implementations
     */
    extension?: fhir.Extension[];
    /**
     * Single-valued answer to the question
     */
    valueBoolean?: boolean;
    /**
     * Single-valued answer to the question
     */
    valueDecimal?: fhir.decimal;
    /**
     * Single-valued answer to the question
     */
    valueInteger?: fhir.integer;
    /**
     * Single-valued answer to the question
     */
    valueDate?: fhir.date;
    /**
     * Single-valued answer to the question
     */
    valueDateTime?: fhir.dateTime;
    /**
     * Single-valued answer to the question
     */
    valueTime?: fhir.time;
    /**
     * Single-valued answer to the question
     */
    valueString?: string;
    /**
     * Single-valued answer to the question
     */
    valueUri?: fhir.uri;
    /**
     * Single-valued answer to the question
     */
    valueAttachment?: fhir.Attachment;
    /**
     * Single-valued answer to the question
     */
    valueCoding?: fhir.Coding;
    /**
     * Single-valued answer to the question
     */
    valueQuantity?: fhir.Quantity;
    /**
     * Single-valued answer to the question
     */
    valueReference?: fhir.Reference;
    /**
     * Nested groups and questions
     */
    item?: QuestionnaireResponseItem[];
}

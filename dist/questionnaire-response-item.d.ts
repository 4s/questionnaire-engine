import * as fhir from 'fhir';
import QuestionnaireResponseItemAnswer from './questionnaire-response-item-answer';
/**
 * Class representing an Item for Questionnaire Responses.
 */
export default class QuestionnaireResponseItem implements fhir.QuestionnaireResponseItem {
    /**
     * Pointer to specific item from Questionnaire
     */
    linkId: string;
    /**
     * ElementDefinition - details for the item
     */
    definition?: fhir.uri;
    /**
     * Name for group or question text
     */
    text?: string;
    /**
     * The response(s) to the question
     */
    answer?: QuestionnaireResponseItemAnswer[];
    /**
     * Nested questionnaire response items
     */
    item?: QuestionnaireResponseItem[];
    /**
     * Returns the length of the item-array
     */
    readonly length: number;
    /**
     * Recursively searches through the QuestionnaireResponseItem's item-array, for item with provided linkId
     * @param {String} linkId - The linkId to search for
     * @returns The element if it exists
     */
    findItem(linkId: string): QuestionnaireResponseItem | undefined;
    /**
     * Recursively searches for parent of item with provided linkId
     * @param {String} linkId - The linkId to search for
     * @returns The parent of the item if it exists
     */
    findParent(linkId: string): QuestionnaireResponseItem | undefined;
    /**
     * Adds an item to this item's itemarray
     * @param {QuestionnaireItem} item - The item to add
     * @param {number} [index] - The index of the itemarray at which to add the item (default is 0)
     * @returns A boolean value indicating whether the operation was a success
     */
    addItem(item: QuestionnaireResponseItem, index?: number): boolean;
    /**
     * Replaces this item with the provided item
     * @param {QuestionnaireResponseItem} item - The item to replace this item with
     * @returns {boolean} A boolean value indicating whether the operation was a success
     */
    replaceWith(item: QuestionnaireResponseItem): boolean;
    /**
     * Returns the answer stored in this item, if one exists
     * @param {number} [index] - the index of the answer to be returned (default is 0)
     * @returns {boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity} - The answer requested
     */
    getAnswer(index?: number): boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined;
    /**
     * Returns all answers stored in this item, if any exists
     * @returns {Array} - An array containing all answers for the item with the provided linkId
     */
    getAllAnswers(): boolean[] | fhir.decimal[] | fhir.integer[] | fhir.date[] | fhir.dateTime[] | fhir.time[] | string[] | fhir.uri[] | fhir.Coding[] | fhir.Attachment[] | fhir.Reference[] | fhir.Quantity[];
    /**
     * Sets the answer(s) for this item
     * @param {fhir.QuestionnaireResponseItemAnswer} answer - The answer to be set
     * @param {fhir.Observation} [observation] - An observation to be set along with the answer
     * This parameter is only relevant if the QuestionnaireItem is of type 'observation'.
     * If the type of the QuestionnaireItem is not 'observation', the parameter will be ignored.
     * @param {number} [index] - If multiple answers to this question exists,
     * index can be used to indicate which answer to be set (default is 0)
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    setAnswer(answer: QuestionnaireResponseItemAnswer, observation?: fhir.Observation, index?: number): boolean;
    /**
     * Adds an answer to this item's answer property, if one exists
     * @param {Object} answer - The answer to be added
     * @param {Object} [observation] - An observation to be set along with the answer
     * This parameter is only relevant if the QuestionnaireItem is of type 'observation'.
     * If the type of the QuestionnaireItem is not 'observation', the parameter will be ignored.
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    addAnswer(answer: QuestionnaireResponseItemAnswer, observation?: fhir.Observation): boolean;
    /**
     * Deletes the answer stored in this item
     * @param {number} [index] - The index of the answer to be removed, if more than one exists (default is 0)
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    deleteAnswer(index?: number): boolean;
    /**
     * Deletes all answers stored in this item
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    deleteAllAnswers(): boolean;
    /**
     * Returns the reference(s) to an observation stored in this item's extension-array, if any exist
     * @returns {fhir.Reference[]} The reference(s) to an observation
     */
    getObservationReference(): fhir.Reference[];
    /**
     * Recursively traverses all elements and subelements in item
     * @yields {Object} An object containing two properties: 'value' and 'done' - value is the value to be returned
     * and done is a boolean, indicating whether there is more items to be returned.
     */
    iterator(): IterableIterator<QuestionnaireResponseItem>;
    /**
     * Allows for questionnaire response item to be used in a for..of.. loop
     */
    [Symbol.iterator](): IterableIterator<QuestionnaireResponseItem>;
}

import QuestionnaireParser from './questionnaire-parser';
import Questionnaire from './questionnaire';
import QuestionnaireResponse from './questionnaire-response';
import QuestionnaireItem from './questionnaire-item';
import QuestionnaireResponseItem from './questionnaire-response-item';
import fhir from 'fhir';
/**
 * Class used for parsing FHIR Questionnaires and Questionnaire Responses.
 * @implements {QuestionnaireParser}
 */
export default class FhirParser implements QuestionnaireParser {
    /**
     * Parses FHIR Questionnaire in JSON format to Questionnaire class.
     * @param {Object} questionnaireJson - the JSON to parse to Questionnaire class
     * @returns {Questionnaire} The parsed questionnaire.
     */
    parseQuestionnaire(questionnaireJson: fhir.Questionnaire): Questionnaire;
    /**
     * Parses FHIR QuestionnaireResponse in JSON format to Questionnaire class.
     * @param {Object} questionnaireJson - the JSON to parse to Questionnaire class
     * @returns {Questionnaire} The parsed questionnaire.
     */
    parseQuestionnaireResponse(questionnaireResponseJson: fhir.QuestionnaireResponse): QuestionnaireResponse;
    /**
     * Recursively parses QuestionnaireItems in a nested structure
     * @param {QuestionnaireItem} item - The item to parse
     * @returns {QuestionnaireItem} The parsed item or items
     */
    parseQuestionnaireItem(item: fhir.QuestionnaireItem): QuestionnaireItem;
    /**
     * Recursively parses QuestionnaireResponseItems in a nested structure
     * @param {QuestionnaireResponseItem} item - The item to parse
     * @returns {QuestionnaireResponseItem} The parsed item or items
     */
    parseQuestionnaireResponseItem(item: fhir.QuestionnaireResponseItem): QuestionnaireResponseItem;
}

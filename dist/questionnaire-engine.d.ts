import QuestionnaireParser from './questionnaire-parser';
import QuestionnaireSerializer from './questionnaire-serializer';
import Questionnaire from './questionnaire';
import QuestionnaireResponse from './questionnaire-response';
import QuestionnaireItem from './questionnaire-item';
import QuestionnaireResponseItem from './questionnaire-response-item';
import FhirParser from './fhir-parser';
import FhirSerializer from './fhir-serializer';
import fhir from 'fhir';
/**
 * Class used for handling Questionnaires and Questionnaire Responses.
 * @param {QuestionnaireParser} [questionnareParser] - The questionnaire parser to be used to parse Questionnaires and
 * QuestionnaireResponsesfrom one format into the format used in this QuestionnaireEngine.
 * @param {QuestionnaireSerializer} [questionnaireSerializer] - The questionnaire serializer to be used to serialize
 * Questionnaires and QuestionnaireResponses from the format used in this QuestionnaireEngine into another format.
 * @returns {QuestionnaireEngine}
 */
export default class QuestionnaireEngine {
    /**
     * The parser for Questionnaires and QuestionnaireResponses
     * @private
     * @type { QuestionnaireParser }
     * @ignore
     */
    private questionnaireParser;
    /**
     * The serializer for Questionnaires and QuestionnaireResponses
     * @private
     * @type { QuestionnaireSerializer }
     * @ignore
     */
    private questionnaireSerializer;
    /**
     * The questionnaire currently being manipulated
     * @private
     * @type { Questionnaire }
     * @ignore
     */
    private questionnaire;
    /**
     * The QuestionnaireResponse currently being manipulated
     * @private
     * @type { QuestionnaireResponse }
     * @ignore
     */
    private questionnaireResponse;
    constructor(questionnaireParser?: QuestionnaireParser, questionnaireSerializer?: QuestionnaireSerializer);
    /**
     * Parses questionnaire to the Questionnaire class using the questionnaire parser set
     * @param {Object} questionnaireObject - An object to be parsed into the Questionnaire class
     * @returns {boolean} A boolean indicating whether the operation was successful
     */
    parseQuestionnaire(questionnaireObject: fhir.Questionnaire): boolean;
    /**
     * Parses questionnaire response to the QuestionnaireResponse class using the questionnaire parser set
     * @param {Object} questionnaireResponseObject - An object to be parsed into the Questionnaire class
     * @returns {boolean} A boolean indicating whether the operation was successful
     */
    parseQuestionnaireResponse(questionnaireResponseObject: fhir.QuestionnaireResponse): boolean;
    /**
     * Serializes the questionnaire using the questionnaire serializer set
     * @returns {Object} The serialized questionnaire
     */
    serializeQuestionnaire(): fhir.Questionnaire;
    /**
     * Serializes the questionnaire response using the questionnaire serializer set
     * @returns {Object} The serialized questionnaire
     */
    serializeQuestionnaireResponse(): fhir.QuestionnaireResponse;
    /**
     * Sets the questionnaire parser
     * @param {QuestionnaireParser} questionnaireParser - the questionnaire parser to be set
     * @returns {void}
     */
    setParser(questionnaireParser: QuestionnaireParser): void;
    /**
     * Sets the questionnaire serializer
     * @param {QuestionnaireSerializer} questionnaireSerializer - The questioonnaire serializer to be set
     * @returns {void}
     */
    setSerializer(questionnaireSerializer: QuestionnaireSerializer): void;
    /**
     * Returns the questionnaire
     * @returns {Questionnaire} the questionnaire
     */
    getQuestionnaire(): Questionnaire;
    /**
     * Sets the questionnaire
     * @param {Questionnaire} questionnaire - The questionnaire to be set
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    setQuestionnaire(questionnaire?: Questionnaire): boolean;
    /**
     * Returns the questionnaire response
     * @returns {QuestionnaireResponse} the questionnaire response
     */
    getQuestionnaireResponse(): QuestionnaireResponse;
    /**
     * Sets the questionnaire response
     * @param {QuestionnaireResponse} questionnaireResponse - The questionnaire response to be set
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    setQuestionnaireResponse(questionnaireResponse: QuestionnaireResponse): boolean;
    /**
     * Returns specific item from questionnaire based on linkId, if it exists
     * @param {string} linkId - The id of the item to be returned
     * @returns {Array} The requested item
     */
    findItem(linkId: string): QuestionnaireItem;
    /**
     * Sets specific item in questionnaire and questionnaire response based on linkId
     * @param {string} linkId - The id of the item to be set
     * @param {QuestionnaireItem} item - The item to be set
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    setItem(linkId: string, item: QuestionnaireItem): boolean;
    /**
     * Adds an item to the questionnaire and questionnaire response based on linkId
     * @param {string} linkId - The id of the item to be set
     * @param {QuestionnaireItem} item - The item to be added
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    addItem(item: QuestionnaireItem, parentLinkId: string): boolean;
    /**
     * Deletes specific item from questionnaire and questionnaire response based on linkId
     * @param {string} linkId - The id of the item to be deleted
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    deleteItem(linkId: string): boolean;
    /**
     * Returns the answer in the questionnaire response item based on linkId
     * @param {string} linkId The id of the item for which the answer to be returned
     * @returns {string|number|boolean|Object} - An array containing all answers for the item with the provided linkId
     */
    getAnswer(linkId: string): string | number | boolean | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined;
    /**
     * Returns all answers in the questionnaire response item based on linkId as an array
     * @param {string} linkId The id of the item for which the answer to be returned
     * @returns {Array} - An array containing all answers for the item with the provided linkId
     */
    getAllAnswers(linkId: string): boolean[] | number[] | string[] | fhir.Coding[] | fhir.Attachment[] | fhir.Reference[] | fhir.Quantity[];
    /**
     * Sets the answer in questionnaire response item based on linkId
     * @param {string} linkId - The id of the item for which the answer is to be set
     * @param {boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity} answer - The answer to be set
     * @param {fhir.Observation} [observation] - An observation to be set along with the answer
     * This parameter is only relevant if the QuestionnaireItem is of type 'observation'.
     * If the type of the QuestionnaireItem is not 'observation', the parameter will be ignored.
     * @param {number} [index] - At what index to set the answer (default is 0)
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    setAnswer(linkId: string, answer: boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity, observation?: fhir.Observation, index?: number): boolean;
    /**
     * Replaces all answers for a question with the answers provided in the answers-array
     * @param {string} linkId - The id of the item for which the answers are to be set
     * @param {boolean[] | fhir.decimal[] | fhir.integer[] | fhir.date[] | fhir.dateTime[] | fhir.time[] | string[] | fhir.uri[] | fhir.Coding[] | fhir.Attachment[] | fhir.Reference[] | fhir.Quantity[]} answers - The array of answers to be set
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    setAllAnswers(linkId: string, answers: boolean[] | fhir.decimal[] | fhir.integer[] | fhir.date[] | fhir.dateTime[] | fhir.time[] | string[] | fhir.uri[] | fhir.Coding[] | fhir.Attachment[] | fhir.Reference[] | fhir.Quantity[]): boolean;
    /**
     * Adds an answer to the questionnaire response item based on linkId
     * @param {string} linkId - The id of the item for which the answer is to be added
     * @param {boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity} answer - The answer to be added
     * @param {fhir.Observation} [observation] - An observation to be set along with the answer
     * This parameter is only relevant if the QuestionnaireItem is of type 'observation'.
     * If the type of the QuestionnaireItem is not 'observation', the parameter will be ignored.
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    addAnswer(linkId: string, answer: boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity, observation?: fhir.Observation): boolean;
    /**
     * Deletes answer from questionnaire response based on linkId
     * @param {string} linkId - The id of the item for which the answer is to be deleted
     * @param {number} [index] - The index of the answer to delete, if more than one exists (default is 0)
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    deleteAnswer(linkId: string, index?: number): boolean;
    /**
     * Deletes all answers from questionnaire response based on linkId
     * @param {string} linkId - The id of the item for which all answers are to be deleted
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    deleteAllAnswers(linkId: string): boolean;
    /**
     * Returns true if the question identified by the linkId is enabled
     * @param linkId The linkId of the question to check
     * @returns {boolean} A boolean value indicating whether the item is enabled
     */
    isEnabled(linkId: string): boolean;
    /**
     * Creates a new questionnaire response from the questionnaire
     * @param {Questionnaire} questionnaire - the questionnaire from which to create the questionnaire response
     * @returns {QuestionnaireResponse}
     */
    questionnaireResponseFromQuestionnaire(questionnaire: Questionnaire): QuestionnaireResponse;
    /**
     * Creates a new questionnaire response item from the questionnaire item
     * @param {QuestionnaireItem} questionnaireItem - the questionnaire item from which to create the questionnaire response item
     * @returns {QuestionnaireResponseItem}
     */
    questionnaireResponseItemFromQuestionnaireItem(questionnaireItem: QuestionnaireItem): QuestionnaireResponseItem;
}
export { QuestionnaireParser, QuestionnaireSerializer, Questionnaire, QuestionnaireResponse, QuestionnaireItem, QuestionnaireResponseItem, FhirParser, FhirSerializer };

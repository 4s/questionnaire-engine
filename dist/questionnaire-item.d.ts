import fhir from 'fhir';
/**
 * Class representing an Item for Questionnaires.
 * @class QuestionnaireItem
 * @classdesc Class representing an Item for Questionnaires.
 * @description Creates a QuestionnaireItem
 */
export default class QuestionnaireItem implements fhir.QuestionnaireItem {
    /**
     * Additional Content defined by implementations
     */
    extension?: fhir.Extension[];
    /**
     * Unique id for item in questionnaire
     */
    linkId: string;
    /**
     * ElementDefinition - details for the item
     */
    definition?: fhir.uri;
    /**
     * Corresponding concept for this item in a terminology
     */
    code?: fhir.Coding[];
    /**
     * A short label for a particular group, question or set of display text within the questionnaire.
     */
    prefix?: string;
    /**
     * Primary text for the item
     */
    text?: string;
    /**
     * The type of questionnaire item this is. (group | display | boolean | decimal | integer | date | dateTime +)
     */
    type: fhir.code;
    /**
     * Only allow data when
     */
    enableWhen?: fhir.QuestionnaireItemEnableWhen[];
    /**
     *  Controls how multiple enableWhen values are interpreted - whether all or any must be true. (all | any)
     */
    enableBehavior?: fhir.QuestionnaireItemEnableBehavior;
    /**
     * Whether the item must be included in data results
     */
    required?: boolean;
    /**
     * Whether the item may repeat
     */
    repeats?: boolean;
    /**
     * Don't allow human editing
     */
    readOnly?: boolean;
    /**
     * No more than this many characters
     */
    maxLength?: fhir.integer;
    /**
     * Valueset containing permitted answers
     */
    answerValueSet?: fhir.canonical;
    /**
     * Permitted answer
     */
    answerOption?: fhir.QuestionnaireItemAnswerOption[];
    /**
     * Initial value(s) when item is first rendered
     */
    initial?: fhir.QuestionnaireItemInitial[];
    /**
     * Nested questionnaire items
     */
    item?: QuestionnaireItem[];
    /**
     * Recursively searches through the QuestionnaireItem's item-array, for an item with provided linkId
     * @param {String} linkId - The linkId to search for
     * @returns {QuestionnaireItem} The element if it exists
     */
    findItem(linkId: string): QuestionnaireItem | undefined;
    /**
     * Recursively searches for parent of item with provided linkId
     * @param {String} linkId - The linkId to search for
     * @returns {QuestionnaireItem} The parent of the item if it exists
     */
    findParent(linkId: string): QuestionnaireItem | undefined;
    /**
     * Adds an item to this item's itemarray
     * @param {QuestionnaireItem} item - The item to add
     * @returns {boolean} A boolean value indicating whether the operation was a success
     */
    addItem(item: QuestionnaireItem): boolean;
    /**
     * Replaces this item with the provided item
     * @param {QuestionnaireItem} item - The item to replace this item with
     * @returns {boolean} A boolean value indicating whether the operation was a success
     */
    replaceWith(item: QuestionnaireItem): boolean;
    /**
     * Returns the type of the QuestionnaireItem
     * @returns {string} The type of the QuestionnaireItem
     */
    getType(): string | undefined;
    /**
     * Returns a reference to an ObservationDefinition, if one exists on the item.
     * An ObservaitonDefinition can only be returned, if the item is of type 'observation'
     * @returns {Reference} The reference to the ObservationDefinition
     */
    getObservationDefinitionReference(): fhir.Reference | undefined;
    /**
     * Returns a reference to a DeviceDefinition, if one exists on the item.
     * A DeviceDefinition can only be returned, if the item is of type 'device'
     * @returns {Reference} The reference to the DeviceDefinition
     */
    getDeviceDefinitionReference(): fhir.Reference | undefined;
    /**
     * Recursively traverses all elements and subelements in itemarray
     * @yields {Object} An object containing two properties: 'value' and 'done' - value is the value to be returned
     * and done is a boolean, indicating whether there is more items to be returned.
     */
    iterator(): IterableIterator<QuestionnaireItem>;
    /**
     * Allows for questionnaire item to be used in a for..of.. loop
     */
    [Symbol.iterator](): IterableIterator<QuestionnaireItem>;
}

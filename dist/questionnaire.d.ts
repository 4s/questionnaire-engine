import { QuestionnaireItem } from './questionnaire-engine';
import * as fhir from 'fhir';
/**
 * Class representing a Questionnaire.
 */
export default class Questionnaire implements fhir.Questionnaire {
    /**
     * The type of the resource.
     */
    resourceType: fhir.code;
    /**
     * Contained, inline Resources
     */
    contained?: fhir.Resource[];
    /**
     * Canonical identifier for this questionnaire, represented as a URI (globally unique)
     */
    url?: fhir.uri;
    /**
     * Additional identifier for the questionnaire
     */
    identifier?: fhir.Identifier[];
    /**
     * Business version of the questionnaire
     */
    version?: string;
    /**
     * Name for this questionnaire (computer friendly)
     */
    name?: string;
    /**
     * Name for this questionnaire (human friendly)
     */
    title?: string;
    /**
     * Instantiates protocol or definition
     */
    derivedFrom?: fhir.canonical;
    /**
     * The status of this questionnaire. (draft | active | retired | unknown)
     */
    status: fhir.QuestionnaireStatus;
    /**
     * For testing purposes, and not real usage
     */
    experimental?: boolean;
    /**
     * Resource that can be subject of QuestionnaireResponse
     */
    subjectType?: fhir.code[];
    /**
     * Date last changed
     */
    date?: fhir.dateTime;
    /**
     * Name of the publisher (organization or individual)
     */
    publisher?: string;
    /**
     * Contact details for the publisher
     */
    contact?: fhir.markdown[];
    /**
     * Natural language description of the questionnaire
     */
    description?: fhir.markdown;
    /**
     * The context that the content is intended to support
     */
    useContext?: fhir.UsageContext[];
    /**
     * Intended jurisdiction for questionnaire (if applicable)
     */
    jurisdiction?: fhir.CodeableConcept[];
    /**
     * Why this questionnaire is defined
     */
    purpose?: fhir.markdown;
    /**
     * Use and/or publishing restrictions
     */
    copyright?: fhir.markdown;
    /**
     * When the questionnaire was approved by publisher
     */
    approvalDate?: fhir.date;
    /**
     * When the questionnaire was last reviewed
     */
    lastReviewDate?: fhir.date;
    /**
     * When the questionnaire is expected to be used
     */
    effectivePeriod?: fhir.Period;
    /**
     * Concept that represents the overall questionnaire
     */
    code?: fhir.Coding[];
    /**
     * Questions and sections within the Questionnaire
     */
    item?: QuestionnaireItem[];
    constructor();
    /**
     * Recursively searches through the Questionnaire's item-array, for item with provided linkId
     * @param {String} linkId - The linkId to search for
     * @returns {QuestionnaireItem} The element if it exists
     */
    findItem(linkId: string): QuestionnaireItem | undefined;
    /**
     * Recursively searches through the Questionnaire's item-array, for parent of item with provided linkId
     * @param {String} linkId - The linkId to search for
     * @returns {QuestionnaireItem} The parent of the item if it exists
     */
    findParent(linkId: string): QuestionnaireItem | Questionnaire | undefined;
    /**
     * Sets specific item in questionnaire based on linkId
     * @param {string} linkId - The id of the item to be changed to the provided item
     * @param {QuestionnaireItem} item - The item to be set
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    setItem(linkId: string, item: QuestionnaireItem): boolean;
    /**
     * Adds an item to the questionnaire based on the parents linkId (default is toplevel)
     * @param {QuestionnaireItem} item - The item to be added
     * @param {string} [parentLinkId] - The id of the parent to which the item should be added. (default is toplevel)
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    addItem(item: QuestionnaireItem, parentLinkId?: string): boolean;
    /**
     * Deletes specific item from questionnaire based on linkId
     * @param {string} linkId - The id of the item to be deleted
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    deleteItem(linkId: string): boolean;
    /**
     * Returns resource from the questionnaires contained-array, matching the provided id, if it exists.
     * @param {string} id - ID of the resource to return
     * @returns {Object} The resource if it exists
     */
    getContainedResourceById(id: string): fhir.Resource | undefined;
    /**
     * Returns a reference to a DeviceDefinition for an item specified by a linkId.
     * A DeviceDefinition can be returned if either the item itself is of type 'device' or one of it ancestors is of type
     * device.
     * @param {string} linkId - The linkId for the item, for which to find a DeviceDefinition
     * @returns {Reference} - The reference to the DeviceDefinition
     */
    getDeviceDefinitionReferenceForItem(linkId: string): fhir.Reference | undefined;
    /**
     * Recursively traverses all elements and subelements in itemarray
     * @param {number} [i] - Starting index for traversing (default is 0)
     * @yields {IteratorObject} An object containing two properties: 'value' and 'done' - value is the value to be returned
     * and done is a boolean, indicating whether there are more items to be returned.
     */
    iterator(): IterableIterator<QuestionnaireItem>;
    /**
     * Allows for questionnaire to be used in a for..of.. loop
     * @method
     * @memberof Questionnaire
     */
    [Symbol.iterator](): IterableIterator<QuestionnaireItem>;
}

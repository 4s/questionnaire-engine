import QuestionnaireResponseItem from './questionnaire-response-item';
import * as fhir from 'fhir';
/**
 * Class representing a Questionnaire Response.
 */
export default class QuestionnaireResponse implements QuestionnaireResponse {
    /**
     * The type of the resource.
     */
    resourceType: fhir.code;
    /**
     * Unique id for this set of answers
     */
    identifier?: fhir.Identifier;
    /**
     * Request fulfilled by this QuestionnaireResponse
     */
    basedOn?: fhir.Reference[];
    /**
     * Part of this action
     */
    partOf?: fhir.Reference[];
    /**
     * Form being answered
     */
    questionnaire?: fhir.canonical;
    /**
     * in-progress | completed | amended | entered-in-error | stopped
     */
    status: fhir.code;
    /**
     * The subject of the questions
     */
    subject?: fhir.Reference;
    /**
     * Encounter or Episode during which questionnaire was completed
     */
    context?: fhir.Reference;
    /**
     * Date the answers were gathered
     */
    authored?: fhir.dateTime;
    /**
     * Person who received and recorded the answers
     */
    author?: fhir.Reference;
    /**
     * The person who answered the questions
     */
    source?: fhir.Reference;
    /**
     * Groups and questions
     */
    item?: QuestionnaireResponseItem[];
    constructor();
    /**
     * Returns the length of the item-array
     */
    readonly length: number;
    /**
     * Recursively searches through the QuestionnaireResponse's item-array, for item with provided linkId
     * @param {String} linkId - The linkId to search for
     * @returns {QuestionnaireResponseItem} The element if it exists
     */
    findItem(linkId: string): QuestionnaireResponseItem | undefined;
    /**
     * Recursively searches through the QuestionnaireResponse's item-array, for parent of item with provided linkId
     * @param {String} linkId - The linkId to search for
     * @returns {QuestionnaireResponseItem} The parent of the item if it exists
     */
    findParent(linkId: string): QuestionnaireResponseItem | QuestionnaireResponse | undefined;
    /**
     * Sets specific item in questionnaire response based on linkId
     * @param {string} linkId - The id of the item to be changed to the provided item
     * @param {QuestionnaireResponseItem} item - The item to be set
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    setItem(linkId: string, item: QuestionnaireResponseItem): boolean;
    /**
     * Adds an item to the questionnaire response based on the parents linkId (default is toplevel)
     * @param {string} [parentLinkId] - The id of the parent to which the item should be added. (default is toplevel)
     * @param {string} linkId - The id of the item to be added
     * @param {QuestionnaireResponseItem} item - The item to be added
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    addItem(item: QuestionnaireResponseItem, parentLinkId?: string): boolean;
    /**
     * Deletes specific item from questionnaire response based on linkId
     * @param {string} linkId - The id of the item to be deleted
     * @returns {boolean} A boolean value indicating whether the operation was successful
     */
    deleteItem(linkId: string): boolean;
    /**
     * Recursively traverses all elements and subelements in itemarray
     * @param {number} [i] - Starting index for traversing (default is 0)
     * @yields {Object} An object containing two properties: 'value' and 'done' - value is the value to be returned
     * and done is a boolean, indicating whether there is more items to be returned.
     */
    iterator(): IterableIterator<QuestionnaireResponseItem>;
    /**
     * Allows for questionnaire response to be used in a for..of.. loop
     */
    [Symbol.iterator](): IterableIterator<QuestionnaireResponseItem>;
}

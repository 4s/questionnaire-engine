import QuestionnaireSerializer from './questionnaire-serializer';
import * as fhir from 'fhir';
import { Questionnaire, QuestionnaireResponse, QuestionnaireItem, QuestionnaireResponseItem } from './questionnaire-engine';
/**
 * Class used for serializing FHIR Questionnaires and Questionnaire Responses.
 * @implements {QuestionnaireSerializer}
 */
export default class FhirSerializer implements QuestionnaireSerializer {
    /**
     * Serializes an instance of the Questionnaire class to FHIR Questionnaire in JSON format
     * @param {Questionnaire} questionnaire - The instance of Questionnaire to be serialized
     * @returns {fhir.Questionnaire} The serialized questionnaire
     */
    serializeQuestionnaire(questionnaire: Questionnaire): fhir.Questionnaire;
    /**
     * Serializes an instance of the QuestionnaireResponse class to FHIR QuestionnaireResponse in JSON format
     * @param {QuestionnaireResponse} questionnaireResponse - The instance of QuestionnaireResponse to be serialized
     * @returns {fhir.QuestionnaireResponse} The serialized questionnaire response
     */
    serializeQuestionnaireResponse(questionnaireResponse: QuestionnaireResponse): fhir.QuestionnaireResponse;
    /**
     * Recursively serializes QuestionnaireItem or QuestionnaireItems in a nested structure to items in the FHIR JSON format
     * @param {QuestionnaireItem} item - The QuestionnaireItem to serialize
     * @returns {fhir.QuestionnaireItem} Serialized QuestionnaireItem or QuestionnaireItems
     */
    serializeQuestionnaireItem(item: QuestionnaireItem): fhir.QuestionnaireItem;
    /**
     * Recursively serializes QuestionnaireResponseItem into QuestionnaireResponse items in the FHIR JSON format
     * @param {QuestionnaireResponseItem} item - The QuestionnaireResponseItem to serialize
     * @returns {fhir.QuestionnaireResponseItem} Serialized QuestionnaireResponseItem or QuestionnaireResponseItems
     */
    serializeQuestionnaireResponseItem(item: QuestionnaireResponseItem): fhir.QuestionnaireResponseItem;
    /**
     * Recursively serializes answers to QuestionnaireResponseItems into answers to QuestionnaireResponse items in the FHIR JSON format
     * @param item
     */
    private serializeQuestionnaireResponseItemAnswer;
}

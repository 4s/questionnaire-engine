import * as fhir from 'fhir';
import { Questionnaire, QuestionnaireResponse } from './questionnaire-engine';
/**
 * Interface for classes used to serialize Questionnaires and QuestionnaireResponses
 * @interface QuestionnaireSerializer
 */
export default interface QuestionnaireSerializer {
    /**
     * Serializes questionnaire from Questionnaire Class - meant to be overridden by child classes
     * @method
     * @memberof QuestionnaireSerializer
     * @returns {fhir.Questionnaire}
     */
    serializeQuestionnaire(questionnaire: Questionnaire): fhir.Questionnaire;
    /**
     * Serializes questionnaire response from QuestionnaireResponse class - meant to be overridden by child classes
     * @method
     * @memberof QuestionnaireSerializer
     * @returns {fhir.QuestionnaireResponse}
     */
    serializeQuestionnaireResponse(questionnaireResponse: QuestionnaireResponse): fhir.QuestionnaireResponse;
}

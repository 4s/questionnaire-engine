import fhir from 'fhir';
/**
 * Functions used for resolving whether a quesiton is enabled
 */
export declare namespace enableWhenUtil {
    /**
     * Returns a function that accepts one or two arguments and evaluates them based on the provided operator as a string
     * @param {string } operator the operator
     * @returns the function
     */
    const stringToOperator: (operator?: string | undefined) => (type: string, answer?: string | number | boolean | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined, expected?: string | number | boolean | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined) => boolean;
    /**
     * Returns a number indicating which of the two arguments are larger.
     * If first argument is larger, a number higher than 0 will be returned.
     * If second argument is larger, a number lower than 0 will be returned
     * If arguments are equal, 0 will be returned
     * @param { string } type The datatype
     * @param { QuestionnaireAnswer } argument The first argument
     * @param { QuestionnaireAnswer } otherArgument The second argument
     */
    const compare: (type: string, argument?: string | number | boolean | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined, otherArgument?: string | number | boolean | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined) => number | undefined;
    /**
     * All possible datatypes for an answer to a question in a FHIR Questionnaire
     */
    type QuestionnaireAnswer = boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity;
}

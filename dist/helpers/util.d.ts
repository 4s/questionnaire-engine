/**
 * Utility functions for Questionnaire Engine
 */
export declare namespace util {
    /**
     * newGuid generates and returns a new v4 GUID
     */
    const newGuid: () => string;
    /**
     * replaceFields replaces all fields on replacee with the fields from replacer
     * @param replacee the object on which the fields should be replaced
     * @param replacer the object containing the field that should replace the fields on the replacee
     */
    const replaceFields: (replacee: any, replacer: any) => void;
    /**
     * removeEmptyFields removes all empty fields from the provided object
     * @param object
     */
    const removeEmptyFields: (object: any) => void;
}

/**
 * Exception used when the item was not found
 * @description Creates new NotFoundException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class NotFoundException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when more than one item was found
 * @description Creates new MoreThanOneFoundException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class MoreThanOneFoundException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when no answer was found on the QuestionnaireResponseItem
 * @description Creates new NoAnswerException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class NoAnswerException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when the QuestionnaireItem or QuestionnaireResponse item does not contain the required linkId
 * @description Creates new NoLinkIdException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class NoLinkIdException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when there was a syntax error
 * @description Creates new SyntaxException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class SyntaxException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when the type is unknown to the library
 * @description Creates new UnknownTypeException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class UnknownTypeException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an error occurs while deleting
 * @description Creates new DeleteException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class DeleteException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when the provided object is not of the type requested
 * @description Creates new TypeException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class TypeException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when a required property is missing from the provided object
 * @description Creates new MissingPropertyException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class MissingPropertyException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when the item already exists
 * @description Creates new ItemAlreadyExists
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class ItemAlreadyExistsException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an attempt to use a parser is made, but no parser is set
 * @description Creates new NoParserException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class NoParserException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an attempt to use an object that does not correctly implement the QuestionnaireParser interface
 * is used in place of a QuesitonnaireParser
 * @description Creates new WrongParserException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class WrongParserException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an error occurs while parsing
 * @description Creates new ParsingException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class ParsingException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an attempt to use a serializer is made, but no serializer is set
 * @description Creates new NoSerializerException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class NoSerializerException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an attempt to use an object that does not correctly implement the QuestionnaireSerializer interface
 * is used in place of a QuestionnaireSerializer
 * @description Creates new WrongSerializerException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class WrongSerializerException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an error occurs while serializing
 * @description Creates new SerializingException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class SerializingException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an attempt to interact with a Questionnaire is made, and no Questionnaire is set
 * @description Creates new NoQuestionnaireException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class NoQuestionnaireException extends Error {
    constructor(message?: string);
}
/**
 * Exception used when an attempt to interact with a QuestionnaireResponse is made, and no QuestionnaireResponse is set
 * @description Creates new NoQuestionnaireResponseException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export declare class NoQuestionnaireResponseException extends Error {
    constructor(message?: string);
}

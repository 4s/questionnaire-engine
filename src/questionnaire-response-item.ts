import { TypeException, NoAnswerException, SyntaxException, MissingPropertyException } from './exceptions/exceptions';
import { util } from './helpers/util';
import * as fhir from 'fhir'
import QuestionnaireResponseItemAnswer from './questionnaire-response-item-answer';
import { Questionnaire } from './questionnaire-engine';

/**
 * Class representing an Item for Questionnaire Responses.
 */
export default class QuestionnaireResponseItem implements fhir.QuestionnaireResponseItem {

  /**
   * Pointer to specific item from Questionnaire
   */
  public linkId: string = ''
  /**
   * ElementDefinition - details for the item
   */
  public definition?: fhir.uri
  /**
   * Name for group or question text
   */
  public text?: string
  /**
   * The response(s) to the question
   */
  public answer?: QuestionnaireResponseItemAnswer[]
  /**
   * Nested questionnaire response items
   */
  public item?: QuestionnaireResponseItem[]

  /**
   * Returns the length of the item-array
   */
  get length(): number {
    if (!this.item) return 0
    return this.item.length
  }

  /**
   * Recursively searches through the QuestionnaireResponseItem's item-array, for item with provided linkId
   * @param {String} linkId - The linkId to search for
   * @returns The element if it exists
   */
  public findItem(linkId: string): QuestionnaireResponseItem | undefined{
    let foundItem: QuestionnaireResponseItem | undefined;
    if (this.linkId === linkId) foundItem = this;
    else if (this.item) {
      this.item.forEach(item => {
        if (!foundItem) foundItem = item.findItem(linkId);
      });
    } else if (this.answer) {
      this.answer.forEach(answer => {
        if (answer.item) {
          answer.item.forEach(item => {
            if (!foundItem) foundItem = item.findItem(linkId);
          });
        }
      });
    }
    return foundItem;
  }

  /**
   * Recursively searches for parent of item with provided linkId
   * @param {String} linkId - The linkId to search for
   * @returns The parent of the item if it exists
   */
  public findParent(linkId: string): QuestionnaireResponseItem | undefined {
    let foundParent: QuestionnaireResponseItem | undefined;
    if (this.item) {
      this.item.forEach(item => {
        if (!foundParent && item.linkId === linkId) foundParent = this;
        else if (!foundParent) foundParent = item.findParent(linkId);
      });
    } else if (this.answer) {
      this.answer.forEach(answer => {
        if (answer.item) {
          answer.item.forEach(item => {
            if (!foundParent && item.linkId === linkId) foundParent = this;
            else if (!foundParent) foundParent = item.findParent(linkId);
          });
        }
      });
    }
    return foundParent;
  }

  /**
   * Adds an item to this item's itemarray
   * @param {QuestionnaireItem} item - The item to add
   * @param {number} [index] - The index of the itemarray at which to add the item (default is 0)
   * @returns A boolean value indicating whether the operation was a success
   */
  public addItem(item: QuestionnaireResponseItem, index: number = 0): boolean {
    if (!(item instanceof QuestionnaireResponseItem)) {
      throw new TypeException();
    }
    let success = false;
    if (this.answer) {
      if (!this.answer[index]) {
        this.answer[index] = {};
      }
      if (!this.answer[index].item) {
        this.answer[index].item = [];
      }
      // @ts-ignore
      this.answer[index].item.push(item);
      success = true;
    } else if (this.item) {
      this.item.push(item);
      success = true;
    } else {
      throw new MissingPropertyException('In order to add an item to a QuestionnaireResponseItem, the questionnaireResponseItem needs either an item-array or a answer[x].item-array.');
    }
    return success;
  }

  /**
   * Replaces this item with the provided item
   * @param {QuestionnaireResponseItem} item - The item to replace this item with
   * @returns {boolean} A boolean value indicating whether the operation was a success
   */
  public replaceWith(item: QuestionnaireResponseItem): boolean {
    if (!(item instanceof QuestionnaireResponseItem)) {
      throw new TypeException();
    }
    util.replaceFields(this, item)
    return true;
  }
  /**
   * Returns the answer stored in this item, if one exists
   * @param {number} [index] - the index of the answer to be returned (default is 0)
   * @returns {boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity} - The answer requested
   */
  public getAnswer(index: number = 0): boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined {
    if (!this.answer) {
      this.answer = [];
    }
    if (!this.answer[index]) {
      return undefined
    }
    let answer: boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined;
    if (this.answer[index].valueAttachment) {
      answer = this.answer[index].valueAttachment
    } else if (typeof this.answer[index].valueBoolean !== 'undefined') {
      answer = this.answer[index].valueBoolean
    } else if (this.answer[index].valueCoding) {
      answer = this.answer[index].valueCoding
    } else if (this.answer[index].valueDate) {
      answer = this.answer[index].valueDate
    } else if (this.answer[index].valueDateTime) {
      answer = this.answer[index].valueDateTime
    } else if (this.answer[index].valueDecimal) {
      answer = this.answer[index].valueDecimal
    } else if (this.answer[index].valueInteger) {
      answer = this.answer[index].valueInteger
    } else if (this.answer[index].valueQuantity) {
      answer = this.answer[index].valueQuantity
    } else if (this.answer[index].valueReference) {
      answer = this.answer[index].valueReference
    } else if (this.answer[index].valueString) {
      answer = this.answer[index].valueString
    } else if (this.answer[index].valueTime) {
      answer = this.answer[index].valueTime
    } else if (this.answer[index].valueUri) {
      answer = this.answer[index].valueUri
    }
    return answer;
  }

  /**
   * Returns all answers stored in this item, if any exists
   * @returns {Array} - An array containing all answers for the item with the provided linkId
   */
  public getAllAnswers(): boolean[] | fhir.decimal[] | fhir.integer[] | fhir.date[] | fhir.dateTime[] | fhir.time[] | string[] | fhir.uri[] | fhir.Coding[] | fhir.Attachment[] | fhir.Reference[] | fhir.Quantity[] {
    if (!this.answer) {
      this.answer = [] as QuestionnaireResponseItemAnswer[];
    }
    const answerArray: boolean[] | fhir.decimal[] | fhir.integer[] | fhir.date[] | fhir.dateTime[] | fhir.time[] | string[] | fhir.uri[] | fhir.Coding[] | fhir.Attachment[] | fhir.Reference[] | fhir.Quantity[] = [];
    this.answer.forEach((_, index: number) => {
      const answer: boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity | undefined = this.getAnswer(index)
      if (answer) {
        answerArray.push()
      }
    })
    return answerArray;
  }

  /**
   * Sets the answer(s) for this item
   * @param {fhir.QuestionnaireResponseItemAnswer} answer - The answer to be set
   * @param {fhir.Observation} [observation] - An observation to be set along with the answer
   * This parameter is only relevant if the QuestionnaireItem is of type 'observation'.
   * If the type of the QuestionnaireItem is not 'observation', the parameter will be ignored.
   * @param {number} [index] - If multiple answers to this question exists,
   * index can be used to indicate which answer to be set (default is 0)
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public setAnswer(answer: QuestionnaireResponseItemAnswer, observation?: fhir.Observation, index: number = 0): boolean {
    let syntax: boolean = false;
    for (const key in answer) {
      if (key.toString().match(/(value)[A-Z]\w*/g)) {
        syntax = true;
      }
    }
    if (!syntax) {
      throw new SyntaxException('The answer provided for the questionnaire response item should be in the form: { value<*>: <answer> }.');
    }
    if (observation) {
      if (!Array.isArray(observation.identifier) || observation.identifier.length < 1) {
        throw new MissingPropertyException('The observation must have at least one unique identifier');
      }
      const identifier: fhir.Identifier | undefined = observation.identifier.find((ident: fhir.Identifier) => {
        return ident.system === 'urn:ietf:rfc:3986';
      });
      if (!identifier || !identifier.value) {
        throw new MissingPropertyException('The observation must contain an identifier with the system "urn:ietf:rfc:3986" with a UUID as value');
      }
      answer.extension = [{
        url: 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation',
        valueReference: {
          identifier
        }
      }];
    }
    let oldAnswer: QuestionnaireResponseItemAnswer | undefined;
    if (this.answer && this.answer.length > 0) {
      oldAnswer = this.answer[index];
      this.answer[index] = answer;
      if (oldAnswer && oldAnswer.item) {
        this.answer[index].item = oldAnswer.item;
      }
    } else if (this.answer) {
      this.answer[index] = answer;
    } else {
      this.answer = [answer]
    }
    return true;
  }

  /**
   * Adds an answer to this item's answer property, if one exists
   * @param {Object} answer - The answer to be added
   * @param {Object} [observation] - An observation to be set along with the answer
   * This parameter is only relevant if the QuestionnaireItem is of type 'observation'.
   * If the type of the QuestionnaireItem is not 'observation', the parameter will be ignored.
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public addAnswer(answer: QuestionnaireResponseItemAnswer, observation?: fhir.Observation): boolean {
    let syntax = false;
    for (const key in answer) {
      if (key.toString().match(/(value)[A-Z]\w*/g)) {
        syntax = true;
      }
    }
    if (!syntax) {
      throw new SyntaxException('The answer provided for the questionnaire response item should be in the form: { value<*>: <answer> }.');
    }
    if (observation) {
      if (!Array.isArray(observation.identifier) || observation.identifier.length < 1) {
        throw new MissingPropertyException('The observation must have at least one unique identifier');
      }
      const identifier: fhir.Identifier | undefined = observation.identifier.find((ident: fhir.Identifier) => {
        return ident.system === 'urn:ietf:rfc:3986';
      });
      answer.extension = [{
        url: 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation',
        valueReference: {
          identifier
        }
      }];
    }
    if (!this.answer || !Array.isArray(this.answer)) {
      this.answer = [];
    }
    this.answer.push(answer);
    return true;
  }

  /**
   * Deletes the answer stored in this item
   * @param {number} [index] - The index of the answer to be removed, if more than one exists (default is 0)
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public deleteAnswer(index = 0): boolean {
    if (!Array.isArray(this.answer)) {
      throw new SyntaxException('There is a syntax error in the item');
    }
    if (index > this.answer.length - 1) {
      throw new NoAnswerException();
    }
    this.answer.splice(index, 1);
    return true;
  }

  /**
   * Deletes all answers stored in this item
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public deleteAllAnswers() {
    this.answer = [];
    return true;
  }

  /**
   * Returns the reference(s) to an observation stored in this item's extension-array, if any exist
   * @returns {fhir.Reference[]} The reference(s) to an observation
   */
  public getObservationReference(): fhir.Reference[] {
    // if item has no answers return empty array
    if (
      !this.answer || 
      this.answer.length === 0
    ) return []

    const references = this.answer
      // filter for answers that have an extension array
      .filter(answer => answer.extension && answer.extension.length !== 0)
      // map to an array of extension arrays filtered for extensions that have the observation-extension url
      .map(answer => answer.extension!.filter(extension => {
        return extension.url === 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation'
      }))
      // flatten the array
      .reduce((acc, cur) => acc.concat(cur), [])
      // filter for extensions that do not have a valueReference field
      .filter(extension => !!extension.valueReference)
      // map to an array of references
      .map(extension => extension.valueReference!)
    
    return references
  }

  /**
   * Recursively traverses all elements and subelements in item
   * @yields {Object} An object containing two properties: 'value' and 'done' - value is the value to be returned
   * and done is a boolean, indicating whether there is more items to be returned.
   */
  public * iterator(): IterableIterator<QuestionnaireResponseItem> {
    if (this.item) {
      for (const item of this.item) {
        yield item
        yield * item.iterator();
      }
    } else if (this.answer) {
      for (const answer of this.answer) {
        if (answer.item) {
          for (const item of answer.item) {
            yield item
            yield * item.iterator();
          }
        }
      }
    }
    return;
  }

  /**
   * Allows for questionnaire response item to be used in a for..of.. loop
   */
  public [Symbol.iterator](): IterableIterator<QuestionnaireResponseItem> {
    return this.iterator();
  }
}

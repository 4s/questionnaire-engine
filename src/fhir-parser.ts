import QuestionnaireParser from './questionnaire-parser';
import Questionnaire from './questionnaire';
import QuestionnaireResponse from './questionnaire-response';
import QuestionnaireItem from './questionnaire-item';
import QuestionnaireResponseItem from './questionnaire-response-item';
import { TypeException } from './exceptions/exceptions';
import fhir from 'fhir';

/**
 * Class used for parsing FHIR Questionnaires and Questionnaire Responses.
 * @implements {QuestionnaireParser}
 */
export default class FhirParser implements QuestionnaireParser {

  
  /**
   * Parses FHIR Questionnaire in JSON format to Questionnaire class.
   * @param {Object} questionnaireJson - the JSON to parse to Questionnaire class
   * @returns {Questionnaire} The parsed questionnaire.
   */
  public parseQuestionnaire(questionnaireJson: fhir.Questionnaire): Questionnaire {
    if (!questionnaireJson.resourceType || questionnaireJson.resourceType !== 'Questionnaire') {
      throw new TypeException();
    }
    
    const questionnaire: Questionnaire = new Questionnaire();
    Object.assign(questionnaire, questionnaireJson);
    const tempItemArray: QuestionnaireItem[] = (questionnaire.item) ? questionnaire.item : [];
    questionnaire.item = [];
    tempItemArray.forEach(item => {
      questionnaire.addItem(this.parseQuestionnaireItem(item));
    });
    return questionnaire;
  }

  /**
   * Parses FHIR QuestionnaireResponse in JSON format to Questionnaire class.
   * @param {Object} questionnaireJson - the JSON to parse to Questionnaire class
   * @returns {Questionnaire} The parsed questionnaire.
   */
  public parseQuestionnaireResponse(questionnaireResponseJson: fhir.QuestionnaireResponse): QuestionnaireResponse {
    if (!questionnaireResponseJson.resourceType || questionnaireResponseJson.resourceType !== 'QuestionnaireResponse') {
      throw new TypeException();
    }
    const questionnaireResponse: QuestionnaireResponse = new QuestionnaireResponse();
    Object.assign(questionnaireResponse, questionnaireResponseJson);
    const tempItemArray: QuestionnaireResponseItem[] = (questionnaireResponse.item) ? questionnaireResponse.item : [] as QuestionnaireResponseItem[];
    questionnaireResponse.item = [];
    tempItemArray.forEach(item => {
      if (questionnaireResponse.item) {
        questionnaireResponse.item.push(this.parseQuestionnaireResponseItem(item));
      }
    });
    return questionnaireResponse;
  }

  /**
   * Recursively parses QuestionnaireItems in a nested structure
   * @param {QuestionnaireItem} item - The item to parse
   * @returns {QuestionnaireItem} The parsed item or items
   */
  public parseQuestionnaireItem(item: fhir.QuestionnaireItem) {
    const questionnaireItem: QuestionnaireItem = new QuestionnaireItem();
    Object.assign(questionnaireItem, item);
    if (questionnaireItem.item && questionnaireItem.item.length > 0) {
      const itemArray: QuestionnaireItem[] = [];
      questionnaireItem.item.forEach(nestedItem => {
        itemArray.push(this.parseQuestionnaireItem(nestedItem));
      });
      questionnaireItem.item = itemArray;
    }
    return questionnaireItem;
  }

  /**
   * Recursively parses QuestionnaireResponseItems in a nested structure
   * @param {QuestionnaireResponseItem} item - The item to parse
   * @returns {QuestionnaireResponseItem} The parsed item or items
   */
  public parseQuestionnaireResponseItem(item: fhir.QuestionnaireResponseItem) {
    const questionnaireResponseItem: QuestionnaireResponseItem = new QuestionnaireResponseItem();
    Object.assign(questionnaireResponseItem, item);
    if (questionnaireResponseItem.item && questionnaireResponseItem.item.length > 0) {
      const itemArray: QuestionnaireResponseItem[] = [];
      questionnaireResponseItem.item.forEach(nestedItem => {
        itemArray.push(this.parseQuestionnaireResponseItem(nestedItem));
      });
      questionnaireResponseItem.item = itemArray;
    } else if (questionnaireResponseItem.answer && questionnaireResponseItem.answer.length > 0) {
      questionnaireResponseItem.answer.forEach(answer => {
        if (answer.item && answer.item.length > 0) {
          const itemArray: QuestionnaireResponseItem[] = [];
          answer.item.forEach(nestedItem => {
            itemArray.push(this.parseQuestionnaireResponseItem(nestedItem));
          });
          answer.item = itemArray;
        }
      });
    }
    return questionnaireResponseItem;
  }
}

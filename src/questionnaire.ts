import { NotFoundException, DeleteException, MissingPropertyException, ItemAlreadyExistsException } from './exceptions/exceptions';
import { QuestionnaireItem } from './questionnaire-engine';
import * as fhir from 'fhir'

/**
 * Class representing a Questionnaire.
 */
export default class Questionnaire implements fhir.Questionnaire {
    /**
     * The type of the resource.
     */
    public resourceType: fhir.code
    /**
     * Contained, inline Resources
     */
    public contained?: fhir.Resource[]
    /**
     * Canonical identifier for this questionnaire, represented as a URI (globally unique)
     */
    public url?: fhir.uri
    /**
     * Additional identifier for the questionnaire
     */
    public identifier?: fhir.Identifier[]
    /**
     * Business version of the questionnaire
     */
    public version?: string
    /**
     * Name for this questionnaire (computer friendly)
     */
    public name?: string
    /**
     * Name for this questionnaire (human friendly)
     */
    public title?: string
    /**
     * Instantiates protocol or definition
     */
    public derivedFrom?: fhir.canonical
    /**
     * The status of this questionnaire. (draft | active | retired | unknown)
     */  
    public status: fhir.QuestionnaireStatus
    /**
     * For testing purposes, and not real usage
     */
    public experimental?: boolean
    /**
     * Resource that can be subject of QuestionnaireResponse
     */
    public subjectType?: fhir.code[]
    /**
     * Date last changed
     */
    public date?: fhir.dateTime
    /**
     * Name of the publisher (organization or individual)
     */ 
    public publisher?: string
    /**
     * Contact details for the publisher
     */
    public contact?: fhir.markdown[]
    /**
     * Natural language description of the questionnaire
     */
    public description?: fhir.markdown
    /**
     * The context that the content is intended to support
     */
    public useContext?: fhir.UsageContext[]
    /**
     * Intended jurisdiction for questionnaire (if applicable)
     */
    public jurisdiction?: fhir.CodeableConcept[]
    /**
     * Why this questionnaire is defined
     */
    public purpose?: fhir.markdown
    /**
     * Use and/or publishing restrictions
     */
    public copyright?: fhir.markdown
    /**
     * When the questionnaire was approved by publisher
     */
    public approvalDate?: fhir.date
    /**
     * When the questionnaire was last reviewed
     */
    public lastReviewDate?: fhir.date
    /**
     * When the questionnaire is expected to be used
     */
    public effectivePeriod?: fhir.Period
    /**
     * Concept that represents the overall questionnaire
     */
    public code?: fhir.Coding[]
    /**
     * Questions and sections within the Questionnaire
     */
    public item?: QuestionnaireItem[]

  constructor() {
    this.resourceType = 'Questionnaire';
    this.status = 'draft'
  }

  /**
   * Recursively searches through the Questionnaire's item-array, for item with provided linkId
   * @param {String} linkId - The linkId to search for
   * @returns {QuestionnaireItem} The element if it exists
   */
  public findItem(linkId: string): QuestionnaireItem | undefined {
    let foundItem: QuestionnaireItem | undefined;
    if (this.item) {
      this.item.forEach(element => {
        if (!foundItem && element.linkId === linkId) {
          foundItem = element;
        }
        if (!foundItem && element.item) {
          foundItem = element.findItem(linkId);
        }
      });
    }
    return foundItem;
  }

  /**
   * Recursively searches through the Questionnaire's item-array, for parent of item with provided linkId
   * @param {String} linkId - The linkId to search for
   * @returns {QuestionnaireItem} The parent of the item if it exists
   */
  public findParent(linkId: string): QuestionnaireItem | Questionnaire | undefined {
    let foundParent: QuestionnaireItem | Questionnaire | undefined;
    if (this.item) {
      this.item.forEach(element => {
        if (!foundParent && element.linkId === linkId) {
          foundParent = this;
        }
        if (!foundParent && element.item) {
          foundParent = element.findParent(linkId);
        }
      });
    }
    return foundParent;
  }

  /**
   * Sets specific item in questionnaire based on linkId
   * @param {string} linkId - The id of the item to be changed to the provided item
   * @param {QuestionnaireItem} item - The item to be set
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public setItem(linkId: string, item: QuestionnaireItem): boolean {
    const foundItem: QuestionnaireItem | undefined = this.findItem(linkId);
    if (!foundItem) {
      throw new NotFoundException();
    }
    foundItem.replaceWith(item);
    return true;
  }

  /**
   * Adds an item to the questionnaire based on the parents linkId (default is toplevel)
   * @param {QuestionnaireItem} item - The item to be added
   * @param {string} [parentLinkId] - The id of the parent to which the item should be added. (default is toplevel)
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public addItem(item: QuestionnaireItem, parentLinkId?: string): boolean {
    const parentItem = (parentLinkId) ? this.findItem(parentLinkId) : this;
    if (!parentItem) {
      throw new NotFoundException('No item matching the provided parentLinkId');
    }
    if (!item.linkId) {
      throw new MissingPropertyException('Item has no linkId');
    }
    if (this.findItem(item.linkId)) {
      throw new ItemAlreadyExistsException();
    }
    if (parentItem !== this) {
      parentItem.addItem(item);
    } else {
      if (!this.item || !Array.isArray(this.item)) {
        this.item = [];
      }
      this.item.push(item);
    }
    return true;
  }

  /**
   * Deletes specific item from questionnaire based on linkId
   * @param {string} linkId - The id of the item to be deleted
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public deleteItem(linkId: string): boolean {
    const parent: QuestionnaireItem | Questionnaire | undefined = this.findParent(linkId);
    if (!parent) {
      throw new NotFoundException();
    }
    const foundItem = parent.findItem(linkId);
    let index: number = -1;
    if (parent && parent.item && foundItem) index = parent.item.indexOf(foundItem);
    if (index === -1) {
      throw new DeleteException();
    }
    if (parent.item) parent.item.splice(index, 1);
    return true;
  }

  /**
   * Returns resource from the questionnaires contained-array, matching the provided id, if it exists.
   * @param {string} id - ID of the resource to return
   * @returns {Object} The resource if it exists
   */
  public getContainedResourceById(id: string): fhir.Resource | undefined{
    let containedResource: fhir.Resource | undefined;
    if (Array.isArray(this.contained)) {
      this.contained.forEach((resource: fhir.Resource) => {
        if (resource.id && resource.id === id) {
          containedResource = resource;
        }
      });
    }
    return containedResource;
  }

  /**
   * Returns a reference to a DeviceDefinition for an item specified by a linkId.
   * A DeviceDefinition can be returned if either the item itself is of type 'device' or one of it ancestors is of type
   * device.
   * @param {string} linkId - The linkId for the item, for which to find a DeviceDefinition
   * @returns {Reference} - The reference to the DeviceDefinition
   */
  public getDeviceDefinitionReferenceForItem(linkId: string): fhir.Reference | undefined {
    let deviceDefinitionReference: fhir.Reference | undefined;
    const item = this.findItem(linkId);
    if (item && item.getType() === 'observation-group') {
      deviceDefinitionReference = item.getDeviceDefinitionReference();
    } else {
      const parent = this.findParent(linkId);
      if (parent instanceof Questionnaire) return undefined
      if (parent && parent.linkId) {
        deviceDefinitionReference = this.getDeviceDefinitionReferenceForItem(parent.linkId);
      }
    }
    return deviceDefinitionReference;
  }

  /**
   * Recursively traverses all elements and subelements in itemarray
   * @param {number} [i] - Starting index for traversing (default is 0)
   * @yields {IteratorObject} An object containing two properties: 'value' and 'done' - value is the value to be returned
   * and done is a boolean, indicating whether there are more items to be returned.
   */
  public * iterator(): IterableIterator<QuestionnaireItem> {
    if (this.item && this.item.length > 0) {
      for (const item of this.item) {
        yield item;
        yield * item.iterator();
      }
    }
    return;
  }
  /**
   * Allows for questionnaire to be used in a for..of.. loop
   * @method
   * @memberof Questionnaire
   */
  public [Symbol.iterator](): IterableIterator<QuestionnaireItem> {
    return this.iterator();
  }
}

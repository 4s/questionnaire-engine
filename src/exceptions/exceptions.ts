/**
 * Exception used when the item was not found
 * @description Creates new NotFoundException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class NotFoundException extends Error {
  constructor(message: string = 'Nothing matching the provided linkId was found') {
    super(message);
    this.name = 'NotFoundException';
  }
}

/**
 * Exception used when more than one item was found
 * @description Creates new MoreThanOneFoundException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class MoreThanOneFoundException extends Error {
  constructor(message: string = 'More than one object matching the provided linkId found') {
    super(message);
    this.name = 'MoreThanOneFoundException';
  }
}

/**
 * Exception used when no answer was found on the QuestionnaireResponseItem
 * @description Creates new NoAnswerException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class NoAnswerException extends Error {
  constructor(message: string = 'The item matching the linkId does not contain an answer array') {
    super(message);
    this.name = 'NoAnswerException';
  }
}

/**
 * Exception used when the QuestionnaireItem or QuestionnaireResponse item does not contain the required linkId
 * @description Creates new NoLinkIdException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class NoLinkIdException extends Error {
  constructor(message: string = 'The item does not contain a linkId') {
    super(message);
    this.name = 'NoLinkIdException';
  }
}

/**
 * Exception used when there was a syntax error
 * @description Creates new SyntaxException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class SyntaxException extends Error {
  constructor(message: string = 'The provided object has a syntax error') {
    super(message);
    this.name = 'SyntaxException';
  }
}

/**
 * Exception used when the type is unknown to the library
 * @description Creates new UnknownTypeException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class UnknownTypeException extends Error {
  constructor(message: string = 'The type of questionnaireItem is not automatically compatible with this library.') {
    super(message);
    this.name = 'UnknownTypeException';
  }
}

/**
 * Exception used when an error occurs while deleting
 * @description Creates new DeleteException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class DeleteException extends Error {
  constructor(message: string = 'Something went wrong while deleting') {
    super(message);
    this.name = 'DeleteException';
  }
}

/**
 * Exception used when the provided object is not of the type requested
 * @description Creates new TypeException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class TypeException extends Error {
  constructor(message: string = 'The provided object is not of the right type') {
    super(message);
    this.name = 'TypeException';
  }
}

/**
 * Exception used when a required property is missing from the provided object
 * @description Creates new MissingPropertyException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class MissingPropertyException extends Error {
  constructor(message: string = 'Properties are missing from the object') {
    super(message);
    this.name = 'MissingPropertyException';
  }
}

/**
 * Exception used when the item already exists
 * @description Creates new ItemAlreadyExists
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class ItemAlreadyExistsException extends Error {
  constructor(message: string = 'The item to be added already exists') {
    super(message);
    this.name = 'ItemAlreadyExistsException';
  }
}

/**
 * Exception used when an attempt to use a parser is made, but no parser is set
 * @description Creates new NoParserException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class NoParserException extends Error {
  constructor(message: string = 'No parser set') {
    super(message);
    this.name = 'NoParserException';
  }
}

/**
 * Exception used when an attempt to use an object that does not correctly implement the QuestionnaireParser interface
 * is used in place of a QuesitonnaireParser
 * @description Creates new WrongParserException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class WrongParserException extends Error {
  constructor(message: string = 'The parser needs to implement both the parseQuestionnaire and parseQuestionnaireResponse methods') {
    super(message);
    this.name = 'WrongParserException';
  }
}

/**
 * Exception used when an error occurs while parsing
 * @description Creates new ParsingException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class ParsingException extends Error {
  constructor(message: string = 'Something went wrong while parsing') {
    super(message);
    this.name = 'ParsingException';
  }
}

/**
 * Exception used when an attempt to use a serializer is made, but no serializer is set
 * @description Creates new NoSerializerException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class NoSerializerException extends Error {
  constructor(message: string = 'No serializer set') {
    super(message);
    this.name = 'NoSerializerException';
  }
}

/**
 * Exception used when an attempt to use an object that does not correctly implement the QuestionnaireSerializer interface
 * is used in place of a QuestionnaireSerializer
 * @description Creates new WrongSerializerException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class WrongSerializerException extends Error {
  constructor(message: string = 'The serializer needs to implement both the serializeQuestionnaire and serializeQuestionnaireResponse methods') {
    super(message);
    this.name = 'WrongSerializerException';
  }
}

/**
 * Exception used when an error occurs while serializing
 * @description Creates new SerializingException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class SerializingException extends Error {
  constructor(message: string = 'Something went wrong while serializing') {
    super(message);
    this.name = 'SerializingException';
  }
}

/**
 * Exception used when an attempt to interact with a Questionnaire is made, and no Questionnaire is set
 * @description Creates new NoQuestionnaireException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class NoQuestionnaireException extends Error {
  constructor(message: string = 'No questionnaire') {
    super(message);
    this.name = 'NoQuestionnaireException';
  }
}

/**
 * Exception used when an attempt to interact with a QuestionnaireResponse is made, and no QuestionnaireResponse is set
 * @description Creates new NoQuestionnaireResponseException
 * @param { string } [message] - The message to be contained in the message, instead of the default message.
 */
export class NoQuestionnaireResponseException extends Error {
  constructor(message: string = 'No questionnaire response') {
    super(message);
    this.name = 'NoQuestionnaireResponseException';
  }
}
import {
  NotFoundException,
  DeleteException,
  MissingPropertyException,
  ItemAlreadyExistsException
} from './exceptions/exceptions';
import QuestionnaireResponseItem from './questionnaire-response-item';
import * as fhir from 'fhir'

/**
 * Class representing a Questionnaire Response.
 */
export default class QuestionnaireResponse implements QuestionnaireResponse {

  /**
   * The type of the resource.
   */
  public resourceType: fhir.code
  /**
   * Unique id for this set of answers
   */
  public identifier?: fhir.Identifier
  /**
   * Request fulfilled by this QuestionnaireResponse
   */
  public basedOn?: fhir.Reference[]
  /**
   * Part of this action
   */
  public partOf?: fhir.Reference[]
  /**
   * Form being answered
   */
  public questionnaire?: fhir.canonical
  /**
   * in-progress | completed | amended | entered-in-error | stopped
   */
  public status: fhir.code
  /**
   * The subject of the questions
   */
  public subject?: fhir.Reference
  /**
   * Encounter or Episode during which questionnaire was completed
   */
  public context?: fhir.Reference
  /**
   * Date the answers were gathered
   */
  public authored?: fhir.dateTime
  /**
   * Person who received and recorded the answers
   */
  public author?: fhir.Reference
  /**
   * The person who answered the questions
   */
  public source?: fhir.Reference
  /**
   * Groups and questions
   */
  public item?: QuestionnaireResponseItem[]

  constructor() {
    this.resourceType = 'QuestionnaireResponse'
    this.status = 'in-progress'
  }

  /**
   * Returns the length of the item-array
   */
  get length(): number {
    if (!this.item) return 0
    return this.item.length
  }

  /**
   * Recursively searches through the QuestionnaireResponse's item-array, for item with provided linkId
   * @param {String} linkId - The linkId to search for
   * @returns {QuestionnaireResponseItem} The element if it exists
   */
  public findItem(linkId: string): QuestionnaireResponseItem | undefined {
    let foundItem: QuestionnaireResponseItem | undefined;
    if (this.item) {
      this.item.forEach(item => {
        if (!foundItem) {
          foundItem = item.findItem(linkId);
        }
      });
    } 
    return foundItem;
  }

  /**
   * Recursively searches through the QuestionnaireResponse's item-array, for parent of item with provided linkId
   * @param {String} linkId - The linkId to search for
   * @returns {QuestionnaireResponseItem} The parent of the item if it exists
   */
  public findParent(linkId: string): QuestionnaireResponseItem | QuestionnaireResponse | undefined {
    let foundParent: QuestionnaireResponseItem | QuestionnaireResponse | undefined;
    if (this.item) {
      this.item.forEach(item => {
        if (!foundParent && item.linkId === linkId) {
          foundParent = this;
        }
        if (!foundParent) foundParent = item.findParent(linkId);
      });
    } 
    return foundParent;
  }

  /**
   * Sets specific item in questionnaire response based on linkId
   * @param {string} linkId - The id of the item to be changed to the provided item
   * @param {QuestionnaireResponseItem} item - The item to be set
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public setItem(linkId: string, item: QuestionnaireResponseItem) {
    const foundItem: QuestionnaireResponseItem | undefined = this.findItem(linkId);
    if (!foundItem) {
      throw new NotFoundException();
    }
    foundItem.replaceWith(item);
    return true;
  }

  /**
   * Adds an item to the questionnaire response based on the parents linkId (default is toplevel)
   * @param {string} [parentLinkId] - The id of the parent to which the item should be added. (default is toplevel)
   * @param {string} linkId - The id of the item to be added
   * @param {QuestionnaireResponseItem} item - The item to be added
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public addItem(item: QuestionnaireResponseItem, parentLinkId?: string) {
    const parentItem = (parentLinkId) ? this.findItem(parentLinkId) : this;
    if (!parentItem) {
      throw new NotFoundException('No item matching the provided parentLinkId');
    }
    if (!item.linkId) {
      throw new MissingPropertyException('Item has no linkId');
    }
    if (this.findItem(item.linkId)) {
      throw new ItemAlreadyExistsException();
    }
    if (parentItem !== this) {
      parentItem.addItem(item);
    } else {
      if (!this.item || !Array.isArray(this.item)) {
        this.item = [];
      }
      this.item.push(item);
    }
    return true;
  }

  /**
   * Deletes specific item from questionnaire response based on linkId
   * @param {string} linkId - The id of the item to be deleted
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public deleteItem(linkId: string): boolean {
    const parentItem: QuestionnaireResponseItem | QuestionnaireResponse | undefined = this.findParent(linkId);
    if (!parentItem) {
      throw new NotFoundException();
    }
    const foundItem: QuestionnaireResponseItem | undefined = parentItem.findItem(linkId);
    let index: number = -1;
    if (parentItem && parentItem.item && foundItem) index = parentItem.item.indexOf(foundItem);
    if (index === -1) {
      throw new DeleteException();
    }
    if (parentItem.item) parentItem.item.splice(index, 1);
    return true;
  }

  /**
   * Recursively traverses all elements and subelements in itemarray
   * @param {number} [i] - Starting index for traversing (default is 0)
   * @yields {Object} An object containing two properties: 'value' and 'done' - value is the value to be returned
   * and done is a boolean, indicating whether there is more items to be returned.
   */
  public * iterator(): IterableIterator<QuestionnaireResponseItem> {
    if (this.item) {
      for (const item of this.item) {
        yield item;
        yield * item.iterator();
      }
    }
    return;
  }

  /**
   * Allows for questionnaire response to be used in a for..of.. loop
   */
  public [Symbol.iterator](): IterableIterator<QuestionnaireResponseItem> {
    return this.iterator();
  }
}

import QuestionnaireParser from './questionnaire-parser';
import QuestionnaireSerializer from './questionnaire-serializer';
import Questionnaire from './questionnaire';
import QuestionnaireResponse from './questionnaire-response';
import QuestionnaireItem from './questionnaire-item';
import QuestionnaireResponseItem from './questionnaire-response-item';
import FhirParser from './fhir-parser';
import FhirSerializer from './fhir-serializer';
import fhir from 'fhir'

import {
  NoParserException,
  ParsingException,
  NoSerializerException,
  SerializingException,
  NoQuestionnaireException,
  UnknownTypeException,
  TypeException,
  NoQuestionnaireResponseException,
  NotFoundException,
  NoLinkIdException
} from './exceptions/exceptions';
import { util } from './helpers/util'
import { enableWhenUtil } from './helpers/enableWhenUtil'
import QuestionnaireResponseItemAnswer from './questionnaire-response-item-answer';

/**
 * Class used for handling Questionnaires and Questionnaire Responses.
 * @param {QuestionnaireParser} [questionnareParser] - The questionnaire parser to be used to parse Questionnaires and
 * QuestionnaireResponsesfrom one format into the format used in this QuestionnaireEngine.
 * @param {QuestionnaireSerializer} [questionnaireSerializer] - The questionnaire serializer to be used to serialize
 * Questionnaires and QuestionnaireResponses from the format used in this QuestionnaireEngine into another format.
 * @returns {QuestionnaireEngine}
 */
export default class QuestionnaireEngine {
  
  /**
   * The parser for Questionnaires and QuestionnaireResponses
   * @private
   * @type { QuestionnaireParser }
   * @ignore
   */
  private questionnaireParser: QuestionnaireParser | undefined
  /**
   * The serializer for Questionnaires and QuestionnaireResponses
   * @private
   * @type { QuestionnaireSerializer }
   * @ignore
   */
  private questionnaireSerializer: QuestionnaireSerializer | undefined
  /**
   * The questionnaire currently being manipulated
   * @private
   * @type { Questionnaire }
   * @ignore
   */
  private questionnaire: Questionnaire | undefined
  /**
   * The QuestionnaireResponse currently being manipulated
   * @private
   * @type { QuestionnaireResponse }
   * @ignore
   */
  private questionnaireResponse: QuestionnaireResponse | undefined

  constructor(questionnaireParser?: QuestionnaireParser, questionnaireSerializer?: QuestionnaireSerializer) {
    this.questionnaireParser = questionnaireParser;
    this.questionnaireSerializer = questionnaireSerializer;
  }

  /**
   * Parses questionnaire to the Questionnaire class using the questionnaire parser set
   * @param {Object} questionnaireObject - An object to be parsed into the Questionnaire class
   * @returns {boolean} A boolean indicating whether the operation was successful
   */
  public parseQuestionnaire(questionnaireObject: fhir.Questionnaire) {
    if (!this.questionnaireParser) {
      throw new NoParserException();
    }
    this.setQuestionnaire(
      this.questionnaireParser.parseQuestionnaire(questionnaireObject)
    );
    if (!this.questionnaire) {
      throw new ParsingException();
    }
    return true;
  }
  /**
   * Parses questionnaire response to the QuestionnaireResponse class using the questionnaire parser set
   * @param {Object} questionnaireResponseObject - An object to be parsed into the Questionnaire class
   * @returns {boolean} A boolean indicating whether the operation was successful
   */
  public parseQuestionnaireResponse(questionnaireResponseObject: fhir.QuestionnaireResponse) {
    if (!this.questionnaireParser) {
      throw new NoParserException();
    }
    this.setQuestionnaireResponse(
      this.questionnaireParser.parseQuestionnaireResponse(
        questionnaireResponseObject
      )
    );
    if (!this.questionnaireResponse) {
      throw new ParsingException();
    }
    return true;
  }

  /**
   * Serializes the questionnaire using the questionnaire serializer set
   * @returns {Object} The serialized questionnaire
   */
  public serializeQuestionnaire() {
    if (!this.questionnaireSerializer) {
      throw new NoSerializerException();
    }
    if (!this.questionnaire) {
      throw new SerializingException();
    }
    return this.questionnaireSerializer.serializeQuestionnaire(
      this.questionnaire
    );
  }

  /**
   * Serializes the questionnaire response using the questionnaire serializer set
   * @returns {Object} The serialized questionnaire
   */
  public serializeQuestionnaireResponse(): fhir.QuestionnaireResponse {
    if (!this.questionnaireSerializer) {
      throw new NoSerializerException();
    }
    if (!this.questionnaireResponse) {
      throw new SerializingException();
    }
    return this.questionnaireSerializer.serializeQuestionnaireResponse(
      this.questionnaireResponse
    );
  }

  /**
   * Sets the questionnaire parser
   * @param {QuestionnaireParser} questionnaireParser - the questionnaire parser to be set
   * @returns {void}
   */
  public setParser(questionnaireParser: QuestionnaireParser) {
    this.questionnaireParser = questionnaireParser;
  }

  /**
   * Sets the questionnaire serializer
   * @param {QuestionnaireSerializer} questionnaireSerializer - The questioonnaire serializer to be set
   * @returns {void}
   */
  public setSerializer(questionnaireSerializer: QuestionnaireSerializer) {
    this.questionnaireSerializer = questionnaireSerializer;
  }

  /**
   * Returns the questionnaire
   * @returns {Questionnaire} the questionnaire
   */
  public getQuestionnaire() {
    if (!this.questionnaire) {
      throw new NoQuestionnaireException();
    }
    return this.questionnaire;
  }

  /**
   * Sets the questionnaire
   * @param {Questionnaire} questionnaire - The questionnaire to be set
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public setQuestionnaire(questionnaire?: Questionnaire) {
    this.questionnaire = questionnaire;
    if (!this.questionnaire) this.questionnaireResponse = undefined
    else {
      if (!this.questionnaireResponse) {
        this.questionnaireResponse = this.questionnaireResponseFromQuestionnaire(
          this.questionnaire
        );
      }
    }
    return true;
  }

  /**
   * Returns the questionnaire response
   * @returns {QuestionnaireResponse} the questionnaire response
   */
  public getQuestionnaireResponse() {
    if (!this.questionnaireResponse) {
      throw new NoQuestionnaireResponseException();
    }
    return this.questionnaireResponse;
  }

  /**
   * Sets the questionnaire response
   * @param {QuestionnaireResponse} questionnaireResponse - The questionnaire response to be set
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public setQuestionnaireResponse(questionnaireResponse: QuestionnaireResponse) {
    if (!(questionnaireResponse instanceof QuestionnaireResponse)) {
      throw new TypeException();
    }
    this.questionnaireResponse = questionnaireResponse;
    return true;
  }

  /**
   * Returns specific item from questionnaire based on linkId, if it exists
   * @param {string} linkId - The id of the item to be returned
   * @returns {Array} The requested item
   */
  public findItem(linkId: string) {
    if (!this.questionnaire) {
      throw new NoQuestionnaireException();
    }
    const item = this.questionnaire.findItem(linkId);
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }
  /**
   * Sets specific item in questionnaire and questionnaire response based on linkId
   * @param {string} linkId - The id of the item to be set
   * @param {QuestionnaireItem} item - The item to be set
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public setItem(linkId: string, item: QuestionnaireItem) {
    if (!this.questionnaire) {
      throw new NoQuestionnaireException();
    }
    let success = this.questionnaire.setItem(linkId, item);
    if ( this.questionnaireResponse && this.questionnaireResponse.findItem(linkId)) {
      success = success && this.questionnaireResponse.setItem(
        linkId, this.questionnaireResponseItemFromQuestionnaireItem(item)
      );
    }
    return success;
  }

  /**
   * Adds an item to the questionnaire and questionnaire response based on linkId
   * @param {string} linkId - The id of the item to be set
   * @param {QuestionnaireItem} item - The item to be added
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public addItem(item: QuestionnaireItem, parentLinkId: string) {
    if (!this.questionnaire) {
      throw new NoQuestionnaireException();
    }
    let success = this.questionnaire.addItem(item, parentLinkId);
    if (this.getQuestionnaireResponse()) {
      success = success && this.getQuestionnaireResponse().addItem(
          this.questionnaireResponseItemFromQuestionnaireItem(item)
        );
    } else {
      success = success && (
        this.questionnaireResponseFromQuestionnaire(this.getQuestionnaire()) !== undefined
      )
    }
    return success;
  }

  /**
   * Deletes specific item from questionnaire and questionnaire response based on linkId
   * @param {string} linkId - The id of the item to be deleted
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public deleteItem(linkId: string) {
    if (!this.questionnaire) {
      throw new NoQuestionnaireException();
    }
    const success = this.questionnaire.deleteItem(linkId);
    if (
      this.questionnaireResponse && this.questionnaireResponse.findItem(linkId)
    ) {
      this.questionnaireResponse.deleteItem(linkId);
    }
    return success;
  }

  /**
   * Returns the answer in the questionnaire response item based on linkId
   * @param {string} linkId The id of the item for which the answer to be returned
   * @returns {string|number|boolean|Object} - An array containing all answers for the item with the provided linkId
   */
  public getAnswer(linkId: string) {
    if (!this.questionnaire) {
      throw new NoQuestionnaireResponseException();
    }
    let item: QuestionnaireResponseItem | undefined;
    if (this.questionnaireResponse) item = this.questionnaireResponse.findItem(linkId);
    if (!item) {
      throw new NotFoundException();
    }
    return item.getAnswer();
  }

  /**
   * Returns all answers in the questionnaire response item based on linkId as an array
   * @param {string} linkId The id of the item for which the answer to be returned
   * @returns {Array} - An array containing all answers for the item with the provided linkId
   */
  public getAllAnswers(linkId: string) {
    if (!this.questionnaire) {
      throw new NoQuestionnaireResponseException();
    }
    let item: QuestionnaireResponseItem | undefined
    if (this.questionnaireResponse) item = this.questionnaireResponse.findItem(linkId);
    if (!item) {
      throw new NotFoundException();
    }
    return item.getAllAnswers();
  }

  /**
   * Sets the answer in questionnaire response item based on linkId
   * @param {string} linkId - The id of the item for which the answer is to be set
   * @param {boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity} answer - The answer to be set
   * @param {fhir.Observation} [observation] - An observation to be set along with the answer
   * This parameter is only relevant if the QuestionnaireItem is of type 'observation'.
   * If the type of the QuestionnaireItem is not 'observation', the parameter will be ignored.
   * @param {number} [index] - At what index to set the answer (default is 0)
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public setAnswer(
    linkId: string, 
    answer: boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity, 
    observation?: fhir.Observation, 
    index: number = 0
  ) {
    const questionnaireItem: QuestionnaireItem | undefined = this.getQuestionnaire().findItem(linkId);
    if (!questionnaireItem) {
      throw new NotFoundException('An item with that linkId was not found in the Questionnaire');
    }
    if (!this.questionnaireResponse) {
      if (this.questionnaire) this.setQuestionnaireResponse(this.questionnaireResponseFromQuestionnaire(this.questionnaire));
    }
    let item: QuestionnaireResponseItem | undefined;
    if (this.questionnaireResponse) item = this.questionnaireResponse.findItem(linkId);
    if (!item) {
      throw new NotFoundException();
    }
    const answerObject = {} as QuestionnaireResponseItemAnswer;
    switch (questionnaireItem.type) {
      case 'boolean':
        answerObject.valueBoolean = answer as boolean;
        break;
      case 'decimal':
        answerObject.valueDecimal = answer as fhir.decimal;
        break;
      case 'integer':
        answerObject.valueInteger = answer as fhir.integer;
        break;
      case 'date':
        answerObject.valueDate = answer as fhir.date;
        break;
      case 'dateTime':
        answerObject.valueDateTime = answer as fhir.dateTime;
        break;
      case 'time':
        answerObject.valueTime = answer as fhir.time;
        break;
      case 'string':
        answerObject.valueString = answer as string;
        break;
      case 'text':
        answerObject.valueString = answer as string;
        break;
      case 'url':
        answerObject.valueUri = answer as fhir.uri;
        break;
      case 'choice':
        answerObject.valueCoding = answer as fhir.Coding;
        break;
      case 'open-choice':
        answerObject.valueCoding = answer as fhir.Coding;
        break;
      case 'attachment':
        answerObject.valueAttachment = answer as fhir.Attachment;
        break;
      case 'reference':
        answerObject.valueReference = answer as fhir.Reference;
        break;
      case 'quantity':
        answerObject.valueQuantity = answer as fhir.Quantity;
        break;
      case 'group':
        throw new UnknownTypeException('Group-items cannot have answers.');
      default:
        throw new UnknownTypeException();
    }

    return item.setAnswer(answerObject, (questionnaireItem.getType() === 'observation') ? observation : undefined, index);
  }

  /**
   * Replaces all answers for a question with the answers provided in the answers-array
   * @param {string} linkId - The id of the item for which the answers are to be set
   * @param {boolean[] | fhir.decimal[] | fhir.integer[] | fhir.date[] | fhir.dateTime[] | fhir.time[] | string[] | fhir.uri[] | fhir.Coding[] | fhir.Attachment[] | fhir.Reference[] | fhir.Quantity[]} answers - The array of answers to be set
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public setAllAnswers(
    linkId: string, 
    answers: boolean[] | fhir.decimal[] | fhir.integer[] | fhir.date[] | fhir.dateTime[] | fhir.time[] | string[] | fhir.uri[] | fhir.Coding[] | fhir.Attachment[] | fhir.Reference[] | fhir.Quantity[]
  ) {
    let success = true;
    success = success && this.deleteAllAnswers(linkId);
    answers.forEach((answer: boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity) => {
      success = success && this.addAnswer(linkId, answer);
    });
    return success;
  }

  /**
   * Adds an answer to the questionnaire response item based on linkId
   * @param {string} linkId - The id of the item for which the answer is to be added
   * @param {boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity} answer - The answer to be added
   * @param {fhir.Observation} [observation] - An observation to be set along with the answer
   * This parameter is only relevant if the QuestionnaireItem is of type 'observation'.
   * If the type of the QuestionnaireItem is not 'observation', the parameter will be ignored.
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public addAnswer(
    linkId: string, 
    answer: boolean | fhir.decimal | fhir.integer | fhir.date | fhir.dateTime | fhir.time | string | fhir.uri | fhir.Coding | fhir.Attachment | fhir.Reference | fhir.Quantity, 
    observation?: fhir.Observation
  ): boolean {
    const  questionnaireItem: QuestionnaireItem | undefined = this.getQuestionnaire().findItem(linkId);
    if (!questionnaireItem) {
      throw new NotFoundException();
    }
    if (!this.questionnaireResponse) {
      if (!this.questionnaire) {
        throw new NoQuestionnaireResponseException();
      } else {
        this.questionnaireResponse = this.questionnaireResponseFromQuestionnaire(
          this.questionnaire
        );
      }
    }
    let item: QuestionnaireResponseItem | undefined;
    if (this.questionnaireResponse) item = this.questionnaireResponse.findItem(linkId);
    if (!item) {
      throw new NotFoundException();
    }

    const answerObject = {} as QuestionnaireResponseItemAnswer;
    switch (questionnaireItem.type) {
      case 'boolean':
        answerObject.valueBoolean = answer as boolean;
        break;
      case 'decimal':
        answerObject.valueDecimal = answer as fhir.decimal;
        break;
      case 'integer':
        answerObject.valueInteger = answer as fhir.integer;
        break;
      case 'date':
        answerObject.valueDate = answer as fhir.date;
        break;
      case 'dateTime':
        answerObject.valueDateTime = answer as fhir.dateTime;
        break;
      case 'time':
        answerObject.valueTime = answer as fhir.time;
        break;
      case 'string':
        answerObject.valueString = answer as string;
        break;
      case 'text':
        answerObject.valueString = answer as string;
        break;
      case 'url':
        answerObject.valueUri = answer as fhir.uri;
        break;
      case 'choice':
        answerObject.valueCoding = answer as fhir.Coding;
        break;
      case 'open-choice':
        answerObject.valueCoding = answer as fhir.Coding
        break;
      case 'attachment':
        answerObject.valueAttachment = answer as fhir.Attachment;
        break;
      case 'reference':
        answerObject.valueReference = answer as fhir.Reference;
        break;
      case 'quantity':
        answerObject.valueQuantity = answer as fhir.Quantity;
        break;
      case 'group':
        throw new UnknownTypeException('Group-items cannot have answers.');
      case 'display':
        throw new UnknownTypeException('Display-items cannot have answers.');
      default:
        throw new UnknownTypeException();
    }
    return item.addAnswer(answerObject, (questionnaireItem.getType() === 'observation') ? observation : undefined);
  }

  /**
   * Deletes answer from questionnaire response based on linkId
   * @param {string} linkId - The id of the item for which the answer is to be deleted
   * @param {number} [index] - The index of the answer to delete, if more than one exists (default is 0)
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public deleteAnswer(linkId: string, index?: number) {
    if (!this.questionnaireResponse) {
      if (!this.questionnaire) {
        throw new NoQuestionnaireResponseException();
      } else {
        this.questionnaireResponse = this.questionnaireResponseFromQuestionnaire(
          this.questionnaire
        );
      }
    }
    let item: QuestionnaireResponseItem | undefined;
    if (this.questionnaireResponse) item = this.questionnaireResponse.findItem(linkId);
    if (!item) {
      throw new NotFoundException();
    }
    return item.deleteAnswer(index);
  }

  /**
   * Deletes all answers from questionnaire response based on linkId
   * @param {string} linkId - The id of the item for which all answers are to be deleted
   * @returns {boolean} A boolean value indicating whether the operation was successful
   */
  public deleteAllAnswers(linkId: string) {
    if (!this.questionnaireResponse) {
      if (!this.questionnaire) {
        throw new NoQuestionnaireResponseException();
      } else {
        this.questionnaireResponse = this.questionnaireResponseFromQuestionnaire(
          this.questionnaire
        );
      }
    }
    let item: QuestionnaireResponseItem | undefined;
    if (this.questionnaireResponse) item = this.questionnaireResponse.findItem(linkId);
    if (!item) {
      throw new NotFoundException();
    }
    return item.deleteAllAnswers();
  }

  /**
   * Returns true if the question identified by the linkId is enabled
   * @param linkId The linkId of the question to check
   * @returns {boolean} A boolean value indicating whether the item is enabled
   */
  public isEnabled(linkId: string): boolean {
    const item = this.findItem(linkId);
    if (!item) throw new NotFoundException();

    if (!item.enableWhen) return true

    let enabled: boolean = item.enableBehavior === 'all'

    for (const enableWhen of item.enableWhen) {
      if (!enableWhen.question) continue;

      let type: string
      let expected: enableWhenUtil.QuestionnaireAnswer
      switch (true) {
        case typeof enableWhen.answerBoolean !== 'undefined':
          expected = enableWhen.answerBoolean as boolean
          type = 'boolean'
          break
        case typeof enableWhen.answerCoding !== 'undefined':
          expected = enableWhen.answerCoding as fhir.Coding
          type = 'Coding'
          break
        case typeof enableWhen.answerDate !== 'undefined':
          expected = enableWhen.answerDate as fhir.date
          type = 'date'
          break
        case typeof enableWhen.answerDateTime !== 'undefined':
          expected = enableWhen.answerDateTime as fhir.dateTime
          type = 'dateTime'
          break
        case typeof enableWhen.answerDecimal !== 'undefined':
          expected = enableWhen.answerDecimal as fhir.decimal
          type = 'decimal'
          break
        case typeof enableWhen.answerInteger !== 'undefined':
          expected = enableWhen.answerInteger as fhir.integer
          type = 'integer'
          break
        case typeof enableWhen.answerQuantity !== 'undefined':
          expected = enableWhen.answerQuantity as fhir.Quantity
          type = 'Quantity'
          break
        case typeof enableWhen.answerReference !== 'undefined':
          expected = enableWhen.answerReference as fhir.Reference
          type = 'Reference'
          break
        case typeof enableWhen.answerString !== 'undefined':
          expected = enableWhen.answerString as string
          type = 'string'
          break
        case typeof enableWhen.answerTime !== 'undefined':
          expected = enableWhen.answerTime as fhir.time
          type = 'time'
          break
        default: 
          // should never happen
          expected = 0,
          type = 'integer'
      }
      // test for NotFoundException??
      if (item.enableBehavior === 'all') {
        enabled = enabled && enableWhenUtil.stringToOperator(enableWhen.operator)(
          type,
          this.getAnswer(enableWhen.question),
          expected
        )
      } else {
        enabled = enabled || enableWhenUtil.stringToOperator(enableWhen.operator)(
          type,
          this.getAnswer(enableWhen.question), 
          expected
        )
      }
    }
    return enabled;
  }

  

  /**
   * Creates a new questionnaire response from the questionnaire
   * @param {Questionnaire} questionnaire - the questionnaire from which to create the questionnaire response
   * @returns {QuestionnaireResponse}
   */
  public questionnaireResponseFromQuestionnaire(questionnaire: Questionnaire) {
    const questionnaireResponse: QuestionnaireResponse = new QuestionnaireResponse();
    // create new identifier
    questionnaireResponse.identifier = {
      system: 'urn:ietf:rfc:3986',
      value: util.newGuid()
    };
    if (questionnaire.url) {
      questionnaireResponse.questionnaire = questionnaire.version ? `${questionnaire.url}|${questionnaire.version}` :  questionnaire.url
    }
    questionnaireResponse.status = 'in-progress';
    questionnaireResponse.item = [];

    // create item list
    if (questionnaire.item) {
      questionnaire.item.forEach(item => {
        questionnaireResponse.addItem(
          this.questionnaireResponseItemFromQuestionnaireItem(item)
        );
      });
    }
    return questionnaireResponse;
  }

  /**
   * Creates a new questionnaire response item from the questionnaire item
   * @param {QuestionnaireItem} questionnaireItem - the questionnaire item from which to create the questionnaire response item
   * @returns {QuestionnaireResponseItem}
   */
  public questionnaireResponseItemFromQuestionnaireItem(questionnaireItem: QuestionnaireItem) {
    const questionnaireResponseItem: QuestionnaireResponseItem = new QuestionnaireResponseItem();
    // copy linkId from questionnaireItem
    if (!questionnaireItem.linkId) {
      throw new NoLinkIdException()
    }
    questionnaireResponseItem.linkId = questionnaireItem.linkId

    // copy definition from questionnaireItem
    if (questionnaireItem.definition) {
      questionnaireResponseItem.definition = questionnaireItem.definition;
    }

    // copy text from questionnaireItem
    if (questionnaireItem.text) {
      questionnaireResponseItem.text = questionnaireItem.text;
    }

    // if item is type 'group' or 'display', add subitems to item array
    if (
      questionnaireItem.type === 'group' ||
      questionnaireItem.type === 'display'
    ) {
      if (questionnaireItem.item) {
        questionnaireResponseItem.item = [];
        questionnaireItem.item.forEach(item => {
          questionnaireResponseItem.addItem(
            this.questionnaireResponseItemFromQuestionnaireItem(item)
          );
        });
      }
    // if item is other type, add subitems to answer's itemarray
    } else {
      questionnaireResponseItem.answer = [];
      if (questionnaireItem.item) {
        const answer: QuestionnaireResponseItemAnswer = {
          item: []
        };
        questionnaireItem.item.forEach(item => {
          if (answer.item) {
            answer.item.push(
              this.questionnaireResponseItemFromQuestionnaireItem(item)
            );
          }
        });
        questionnaireResponseItem.answer.push(answer);
      }
    }
    
    return questionnaireResponseItem;
  }
}

export {
  QuestionnaireParser,
  QuestionnaireSerializer,
  Questionnaire,
  QuestionnaireResponse,
  QuestionnaireItem,
  QuestionnaireResponseItem,
  FhirParser,
  FhirSerializer
};

import QuestionnaireSerializer from './questionnaire-serializer';
import * as fhir from 'fhir'
import { Questionnaire, QuestionnaireResponse, QuestionnaireItem, QuestionnaireResponseItem } from './questionnaire-engine';
import { util } from './helpers/util';
import QuestionnaireResponseItemAnswer from './questionnaire-response-item-answer';

/**
 * Class used for serializing FHIR Questionnaires and Questionnaire Responses.
 * @implements {QuestionnaireSerializer}
 */
export default class FhirSerializer implements QuestionnaireSerializer {

  /**
   * Serializes an instance of the Questionnaire class to FHIR Questionnaire in JSON format
   * @param {Questionnaire} questionnaire - The instance of Questionnaire to be serialized
   * @returns {fhir.Questionnaire} The serialized questionnaire
   */
  public serializeQuestionnaire(questionnaire: Questionnaire): fhir.Questionnaire {
    const questionnaireJson = {} as fhir.Questionnaire;
    Object.assign(questionnaireJson, questionnaire);
    if (questionnaireJson.item && questionnaireJson.item.length > 0) {
      questionnaireJson.item = [];
      if (questionnaire.item && questionnaire.item.length > 0) {
        questionnaire.item.forEach((element: QuestionnaireItem) => {
          if (questionnaireJson.item) {
            questionnaireJson.item.push(this.serializeQuestionnaireItem(element));
          }
        });
      }
    }
    if (questionnaireJson.item && questionnaireJson.item.length === 0) delete questionnaireJson.item
    return questionnaireJson;
  }

  /**
   * Serializes an instance of the QuestionnaireResponse class to FHIR QuestionnaireResponse in JSON format
   * @param {QuestionnaireResponse} questionnaireResponse - The instance of QuestionnaireResponse to be serialized
   * @returns {fhir.QuestionnaireResponse} The serialized questionnaire response
   */
  public serializeQuestionnaireResponse(questionnaireResponse: QuestionnaireResponse): fhir.QuestionnaireResponse {
    const questionnaireResponseJson = {} as fhir.QuestionnaireResponse;
    Object.assign(questionnaireResponseJson, questionnaireResponse);
    if (questionnaireResponseJson.item && questionnaireResponseJson.item.length > 0) {
      questionnaireResponseJson.item = [];
      if (questionnaireResponse.item && questionnaireResponse.item.length > 0) {
        questionnaireResponse.item.forEach((element: QuestionnaireResponseItem) => {
          if (questionnaireResponseJson.item) {
            questionnaireResponseJson.item.push(this.serializeQuestionnaireResponseItem(element));
          }
        });
      }
    }
    if (questionnaireResponseJson.item && questionnaireResponseJson.item.length === 0) delete questionnaireResponseJson.item
    return questionnaireResponseJson;
  }

  /**
   * Recursively serializes QuestionnaireItem or QuestionnaireItems in a nested structure to items in the FHIR JSON format
   * @param {QuestionnaireItem} item - The QuestionnaireItem to serialize
   * @returns {fhir.QuestionnaireItem} Serialized QuestionnaireItem or QuestionnaireItems
   */
  public serializeQuestionnaireItem(item: QuestionnaireItem): fhir.QuestionnaireItem {
    const itemJson = {} as fhir.QuestionnaireItem;
    Object.assign(itemJson, item);
    const tempItemArray = item.item;
    if (tempItemArray && tempItemArray.length > 0) {
      itemJson.item = [];
      tempItemArray.forEach((element: QuestionnaireItem) => {
        if (itemJson.item) itemJson.item.push(this.serializeQuestionnaireItem(element));
      });
    }
    return itemJson;
  }

  /**
   * Recursively serializes QuestionnaireResponseItem into QuestionnaireResponse items in the FHIR JSON format
   * @param {QuestionnaireResponseItem} item - The QuestionnaireResponseItem to serialize
   * @returns {fhir.QuestionnaireResponseItem} Serialized QuestionnaireResponseItem or QuestionnaireResponseItems
   */
  public serializeQuestionnaireResponseItem(item: QuestionnaireResponseItem): fhir.QuestionnaireResponseItem {
    const itemJson = {} as fhir.QuestionnaireResponseItem;
    Object.assign(itemJson, item);
    const tempItemArray = item.item;
    if (tempItemArray && tempItemArray.length > 0) {
      itemJson.item = [];
      tempItemArray.forEach((element: QuestionnaireResponseItem) => {
        if (itemJson.item) itemJson.item.push(this.serializeQuestionnaireResponseItem(element));
      });
    }
    const tempAnswerArray = item.answer;
    if (tempAnswerArray && tempAnswerArray.length > 0) {
      itemJson.answer = [];
      tempAnswerArray.forEach((element: QuestionnaireResponseItemAnswer) => {
        if (itemJson.answer) {
          itemJson.answer.push(this.serializeQuestionnaireResponseItemAnswer(element));
        }
      });
    }
    return itemJson;
  }

  /**
   * Recursively serializes answers to QuestionnaireResponseItems into answers to QuestionnaireResponse items in the FHIR JSON format
   * @param item 
   */
  private serializeQuestionnaireResponseItemAnswer(answer: QuestionnaireResponseItemAnswer): fhir.QuestionnaireResponseItemAnswer {
    const answerJson = {} as fhir.QuestionnaireResponseItem;
    Object.assign(answerJson, answer);
    const tempItemArray = answer.item;
    if (tempItemArray && tempItemArray.length > 0) {
      answerJson.item = [];
      tempItemArray.forEach((element: QuestionnaireResponseItem) => {
        if (answerJson.item) answerJson.item.push(this.serializeQuestionnaireResponseItem(element));
      });
    }
    return answerJson
  }
}

import { Questionnaire, QuestionnaireResponse } from "./questionnaire-engine";
import fhir from "fhir";

/**
 * Interface for classes used to parse Questionnaires and QuestionnaireResponses
 * @interface QuestionnaireParser
 */
export default interface QuestionnaireParser {

  /**
   * Parses questionnaire to Questionnaire Class - meant to be overridden by child classes
   * @method
   * @memberof QuestionnaireParser
   * @returns {Questionnaire}
   */
  parseQuestionnaire(questionnaire: fhir.Questionnaire): Questionnaire

  /**
   * Parses questionnaire response to QuestionnaireResponse class - meant to be overridden by child classes
   * @method
   * @memberof QuestionnaireParser
   * @returns {QuestionnaireResponse}
   */
  parseQuestionnaireResponse(questionnaireResponse: fhir.QuestionnaireResponse): QuestionnaireResponse
}

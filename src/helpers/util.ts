/**
 * Utility functions for Questionnaire Engine
 */
export namespace util {
  /**
   * newGuid generates and returns a new v4 GUID
   */
  export const newGuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      const r: number = (Math.random() * 16) | 0;
      const v: number = c === 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  /**
   * replaceFields replaces all fields on replacee with the fields from replacer
   * @param replacee the object on which the fields should be replaced
   * @param replacer the object containing the field that should replace the fields on the replacee
   */
  export const replaceFields = (replacee: any, replacer: any) => {
    for (const field in replacee) {
      if (replacee.hasOwnProperty(field)) {
        delete replacee[field];
      }
    }
    for (const field in replacer) {
      if (replacer.hasOwnProperty(field)) {
        replacee[field] = replacer[field];
      }
    }
  }

  /**
   * removeEmptyFields removes all empty fields from the provided object
   * @param object 
   */
  export const removeEmptyFields = (object: any) => {
    for (const field in object) {
      if (object.hasOwnProperty(field)) {
        if (!object[field]) {
          delete object[field];
        }
      }
    }
  }
}

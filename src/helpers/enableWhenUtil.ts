import fhir, { Quantity } from 'fhir'
import { UnknownTypeException } from '../exceptions/exceptions';

/**
 * Functions used for resolving whether a quesiton is enabled
 */
export namespace enableWhenUtil {

  /**
   * Returns a function that accepts one or two arguments and evaluates them based on the provided operator as a string
   * @param {string } operator the operator
   * @returns the function
   */
  export const stringToOperator = (operator?: string) => {
    if (!operator) return (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => true
    const map: Map<string, (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => boolean> = new Map([
      ['exists', (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => answer ? true : false],
      ['=', (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => compare(type, answer, expected) === 0],
      ['!=', (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => compare(type, answer, expected) !== 0],
      ['>', (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => {
        const compareValue = compare(type, answer, expected)
        return typeof compareValue !== 'undefined' ? compareValue > 0 : false
      }],
      ['<', (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => {
        const compareValue = compare(type, answer, expected)
        return typeof compareValue !== 'undefined' ? compareValue < 0 : false
      }],
      ['>=', (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => {
        const compareValue = compare(type, answer, expected)
        return typeof compareValue !== 'undefined'  ? compareValue >= 0 : false
      }],
      ['<=', (type: string, answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => {
        const compareValue = compare(type, answer, expected)
        return typeof compareValue !== 'undefined' ? compareValue <= 0 : false
      }]
    ])
    return map.get(operator) || ((answer?: QuestionnaireAnswer, expected?: QuestionnaireAnswer) => true)
  }

  /**
   * Returns a number indicating which of the two arguments are larger. 
   * If first argument is larger, a number higher than 0 will be returned.
   * If second argument is larger, a number lower than 0 will be returned
   * If arguments are equal, 0 will be returned
   * @param { string } type The datatype
   * @param { QuestionnaireAnswer } argument The first argument
   * @param { QuestionnaireAnswer } otherArgument The second argument
   */
  export const compare = (type: string, argument?: QuestionnaireAnswer, otherArgument?: QuestionnaireAnswer) => {
    switch (type) {
      case 'boolean':
        if (typeof argument !== 'boolean' || typeof otherArgument !== 'boolean') return undefined
        return argument === otherArgument ? 0 : undefined
      case 'decimal': case 'integer':
        try {
          return (argument as number) - (otherArgument as number)
        } catch (e) {
          return undefined
        }
      case 'date': case 'dateTime': case 'time':
        try {
          const argumentDate = type === 'time' 
            ? new Date('1970-01-01T' + argument + 'Z') 
            : new Date(argument as string)
          const otherArgumentDate = type === 'time' 
            ? new Date('1970-01-01T' + otherArgument + 'Z') 
            : new Date(otherArgument as string)
          if (argumentDate > otherArgumentDate) return 1
          else if (argumentDate < otherArgumentDate) return -1
          else return 0
        } 
        catch (e) {
          return undefined
        }
      case 'string': case 'uri': 
        try {
          if ((argument as string) > (otherArgument as string)) return 1
          else if ((argument as string) < (otherArgument as string)) return -1
          else return 0
        } catch (e) {
          return undefined
        }
      case 'Coding': 
        try {
          if (
            (argument as fhir.Coding).system === (otherArgument as fhir.Coding).system &&
            (argument as fhir.Coding).code === (otherArgument as fhir.Coding).code
          ) {
            return 0
          } 
          return undefined
        } catch (e) {
          return undefined
        }
      case 'Quantity':
        try {
          const quantity = (argument as Quantity)
          const otherQuantity = (otherArgument as Quantity)
          if (
            (
              quantity.system && 
              otherQuantity.system && 
              quantity.system === otherQuantity.system && 
              quantity.code && 
              otherQuantity.code && 
              quantity.code === otherQuantity.code
            ) ||
            (
              quantity.unit && 
              otherQuantity.unit && 
              quantity.unit === otherQuantity.unit
            )
          ) {
            if (quantity.value && otherQuantity.value) {
              return quantity.value - otherQuantity.value
            }
          }
          return undefined
        } catch (e) {
          return undefined
        }
      case 'Attachment': case 'Reference':
        throw new Error('The question type is not supported by this library\'s enableWhen functionality')
    }
  }

  /**
   * All possible datatypes for an answer to a question in a FHIR Questionnaire
   */
  export type QuestionnaireAnswer =
  | boolean
  | fhir.decimal
  | fhir.integer
  | fhir.date
  | fhir.dateTime
  | fhir.time
  | string
  | fhir.uri
  | fhir.Coding
  | fhir.Attachment
  | fhir.Reference
  | fhir.Quantity
}
import { TypeException, NoAnswerException, SyntaxException, MissingPropertyException } from './exceptions/exceptions';
import { util } from './helpers/util';
import * as fhir from 'fhir'
import { QuestionnaireResponseItem } from './questionnaire-engine';

/**
 * Class representing an Item for Questionnaire Responses.
 */
export default class QuestionnaireResponseItemAnswer implements fhir.QuestionnaireResponseItemAnswer {

    /**
     * Additional Content defined by implementations
     */
    public extension?: fhir.Extension[]
    /**
     * Single-valued answer to the question
     */
    public valueBoolean?: boolean
    /**
     * Single-valued answer to the question
     */
    public valueDecimal?: fhir.decimal
    /**
     * Single-valued answer to the question
     */
    public valueInteger?: fhir.integer
    /**
     * Single-valued answer to the question
     */
    public valueDate?: fhir.date
    /**
     * Single-valued answer to the question
     */
    public valueDateTime?: fhir.dateTime
    /**
     * Single-valued answer to the question
     */
    public valueTime?: fhir.time
    /**
     * Single-valued answer to the question
     */
    public valueString?: string
    /**
     * Single-valued answer to the question
     */
    public valueUri?: fhir.uri
    /**
     * Single-valued answer to the question
     */
    public valueAttachment?: fhir.Attachment
    /**
     * Single-valued answer to the question
     */
    public valueCoding?: fhir.Coding
    /**
     * Single-valued answer to the question
     */
    public valueQuantity?: fhir.Quantity
    /**
     * Single-valued answer to the question
     */
    public valueReference?: fhir.Reference
    /**
     * Nested groups and questions
     */
    public item?: QuestionnaireResponseItem[]
}
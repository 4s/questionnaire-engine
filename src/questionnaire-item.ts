import { TypeException } from './exceptions/exceptions';
import { util } from './helpers/util';
import fhir from 'fhir'


/**
 * Class representing an Item for Questionnaires.
 * @class QuestionnaireItem
 * @classdesc Class representing an Item for Questionnaires.
 * @description Creates a QuestionnaireItem
 */
export default class QuestionnaireItem implements fhir.QuestionnaireItem {

    /**
     * Additional Content defined by implementations
     */
    public extension?: fhir.Extension[]
    /**
     * Unique id for item in questionnaire
     */
    public linkId: string = ''
    /**
     * ElementDefinition - details for the item
     */
    public definition?: fhir.uri
    /**
     * Corresponding concept for this item in a terminology
     */
    public code?: fhir.Coding[]
    /**
     * A short label for a particular group, question or set of display text within the questionnaire.
     */
    public prefix?: string
    /**
     * Primary text for the item
     */
    public text?: string
    /**
     * The type of questionnaire item this is. (group | display | boolean | decimal | integer | date | dateTime +)
     */
    public type: fhir.code = ''
    /**
     * Only allow data when
     */
    public enableWhen?: fhir.QuestionnaireItemEnableWhen[]
    /**
     *  Controls how multiple enableWhen values are interpreted - whether all or any must be true. (all | any)
     */
    public enableBehavior?: fhir.QuestionnaireItemEnableBehavior
    /**
     * Whether the item must be included in data results
     */
    public required?: boolean
    /**
     * Whether the item may repeat
     */
    public repeats?: boolean
    /**
     * Don't allow human editing
     */
    public readOnly?: boolean
    /**
     * No more than this many characters
     */
    public maxLength?: fhir.integer
    /**
     * Valueset containing permitted answers
     */
    public answerValueSet?: fhir.canonical
    /**
     * Permitted answer
     */
    public answerOption?: fhir.QuestionnaireItemAnswerOption[]
    /**
     * Initial value(s) when item is first rendered
     */
    public initial?: fhir.QuestionnaireItemInitial[]
    /**
     * Nested questionnaire items
     */
    public item?: QuestionnaireItem[]


  /**
   * Recursively searches through the QuestionnaireItem's item-array, for an item with provided linkId
   * @param {String} linkId - The linkId to search for
   * @returns {QuestionnaireItem} The element if it exists
   */
  public findItem(linkId: string): QuestionnaireItem | undefined {
    let foundItem: QuestionnaireItem | undefined;
    if (!this.item) this.item = []
    this.item.forEach((element: QuestionnaireItem) => {
      if (!foundItem && element.item) {
        foundItem = element.findItem(linkId);
      }
      if (!foundItem && element.linkId === linkId) {
        foundItem = element;
      }
    });
    return foundItem;
  }

  /**
   * Recursively searches for parent of item with provided linkId
   * @param {String} linkId - The linkId to search for
   * @returns {QuestionnaireItem} The parent of the item if it exists
   */
  public findParent(linkId: string): QuestionnaireItem | undefined {
    let foundParent: QuestionnaireItem | undefined;
    if (!this.item) this.item = []
    this.item.forEach(element => {
      if (!foundParent && element.item) {
        foundParent = element.findParent(linkId);
      }
      if (!foundParent && element.linkId === linkId) {
        foundParent = this;
      }
    });
    return foundParent;
  }

  /**
   * Adds an item to this item's itemarray
   * @param {QuestionnaireItem} item - The item to add
   * @returns {boolean} A boolean value indicating whether the operation was a success
   */
  public addItem(item: QuestionnaireItem) {
    if (!(item instanceof QuestionnaireItem)) {
      throw new TypeException();
    }
    if (!this.item || !Array.isArray(this.item)) {
      this.item = [];
    }
    this.item.push(item);
    return true;
  }

  /**
   * Replaces this item with the provided item
   * @param {QuestionnaireItem} item - The item to replace this item with
   * @returns {boolean} A boolean value indicating whether the operation was a success
   */
  public replaceWith(item: QuestionnaireItem) {
    if (!(item instanceof QuestionnaireItem)) {
      throw new TypeException();
    }
    util.replaceFields(this, item)
    return true;
  }

  /**
   * Returns the type of the QuestionnaireItem
   * @returns {string} The type of the QuestionnaireItem
   */
  public getType(): string | undefined {
    let type: string | undefined = this.type
    if (this.type === 'group') {
      for (const item of this) {
        if (item.getType() === 'observation') type = 'observation-group'
      }
    } else if (this.extension && Array.isArray(this.extension)) {
      this.extension.forEach(extension => {
        if (
          extension.url === 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115539981/Questionnaire+observationDefinition'
        ) {
          type = 'observation'
        }
      });
    } 
    return type;
  }

  /**
   * Returns a reference to an ObservationDefinition, if one exists on the item.
   * An ObservaitonDefinition can only be returned, if the item is of type 'observation'
   * @returns {Reference} The reference to the ObservationDefinition
   */
  public getObservationDefinitionReference(): fhir.Reference | undefined {
    let observationDefinitionReference: fhir.Reference | undefined;
    if (this.getType() !== 'observation') {
      throw new TypeException('Reference to ObservationDefinition can only be retrieved for items with type "observation"');
    }
    if (this.extension && Array.isArray(this.extension)) {
      this.extension.forEach(extension => {
        if (extension.url === 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115539981/Questionnaire+observationDefinition') {
          observationDefinitionReference = extension.valueReference;
        }
      });
    }
    return observationDefinitionReference;
  }

  /**
   * Returns a reference to a DeviceDefinition, if one exists on the item.
   * A DeviceDefinition can only be returned, if the item is of type 'device'
   * @returns {Reference} The reference to the DeviceDefinition
   */
  public getDeviceDefinitionReference(): fhir.Reference | undefined {
    if (this.getType() !== 'observation-group') {
      throw new TypeException('Reference to DeviceDefinition can only be retrieved for items with type "observation-group"');
    }
    let deviceDefinitionReference: fhir.Reference  | undefined;
    if (this.extension && Array.isArray(this.extension)) {
      this.extension.forEach(extension => {
        if (
          extension.url === 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/114720777/Questionnaire+optionalMeasurement' ||
          extension.url === 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/114720801/Questionnaire+mandatoryMeasurement'
        ) {
          deviceDefinitionReference = extension.valueReference;
        }
      });
    }
    return deviceDefinitionReference;
  }

  /**
   * Recursively traverses all elements and subelements in itemarray
   * @yields {Object} An object containing two properties: 'value' and 'done' - value is the value to be returned
   * and done is a boolean, indicating whether there is more items to be returned.
   */
  public * iterator(): IterableIterator<QuestionnaireItem> {
    if (this.item && this.item.length > 0) {
      for (const item of this.item) {
        yield item;
        yield * item.iterator();
      }
    }
    return;
  }

  /**
   * Allows for questionnaire item to be used in a for..of.. loop
   */
  public [Symbol.iterator](): IterableIterator<QuestionnaireItem> {
    return this.iterator();
  }
}

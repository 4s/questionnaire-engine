# Questionnaire Engine

This page will go more into details regarding the purpose, current state, use cases etc. of the Questionnaire Engine
  
[TOC]

## Purpose
This library was build as part of a project entitled "KomTel" (Components for Telehealth or Komponenter til Telesundhed in Danish).

The library is designed to make it easier for developers to create frontend applications that interact with medical questionnaires and questionnaire responses. The project was built on top of FHIR, and as such it comes with FHIR serializers and parsers, that can be used to parse and serialize both FHIR questionnaires and questionnaire responses.

The library allows for easy traversing and editing of FHIR questionnaires, while keeping to the format requirements of the FHIR standard.

## Documentation

This page along with the README.md function as documentation for the library as a whole. Along with this documentation,
an HTML-page containing documentation for all of the methods available in the library can be found in the docs/html
folder of this library. To use it, open the docs/html/index.html page in a browser.

## Maturity level

This service has a maturity level of "Proof of Concept" in relation to 4S maturity level

## Current state

At the time of writing this library allows for general traversing and editing of FHIR questionnaires and questionnaire responses as well as parsing and serializing of these two types of resources.

## Usage

The main class of this library is QuestionnaireEngine.
For the best result, all interaction with questionnaires and questionnaire responses should be done through a QuestionnaireEngine object. Though this is the case, it is possible to edit questionnaires and questionnaire responses directly by retrieving them from the QuestionnaireEngine. If you choose to edit the questionnaires and questionnaire responses directly, this could lead to inconsistencies between the two, and possibly make either questionnaire or questionnaire response invalid, so be careful if choosing to edit them directly.

### Parsing questionnaire from JSON

```js
// Serializer and parser can be set in the constructor
var questionnaireEngine = new QuestionnaireEngine(questionnaireParser, questionnaireSerializer);
questionnaireEngine.parseQuestionnaire(jsonObject);
```

### Setting/Changing the parser and serializer for the questionnaire

```js
questionnaireEngine.setParser(questionnaireParser);
questionnaireEngine.setSerializer(questionnaireSerializer);
```

### Getting a question/item

```js
var linkId = 'linkIdForQuestion'; // Each question (item) in a questionnaire has an ID
var item = questionnaireEngine.getItem(linkId);
```

### Setting a question/item

```js
var linkId = var linkId = 'linkIdForQuestion'; 
var item = new Item( { itemproperties } );
questionnaireEngine.setItem(linkId, item);
```

### Adding a question/item

```js
var item = new Item( { itemproperties } );
questionnaireEngine.addItem(item);
```

### Getting the answer for a question/item

```js
var linkId = 'linkIdForQuestion'; 
var answer = questionnaireEngine.getAnswer(linkId);
```

### Setting the answer for a question/item

```js
var linkId = 'linkIdForQuestion'; 
var answer = 'The answer for the question';
questionnaireEngine.setAnswer(linkId, answer);
```

### Adding an answer for a question/item

```js
var linkId = 'linkIdForQuestion'; 
var answer = 'The answer for the question';
questionnaireEngine.addAnswer(linkId, answer);
```

### Serializing questionnaire to JSON

```js
var jsonObject = questionnaireEngine.serializeQuestionnaire();
```

### Serializing questionnaireResponse to JSON

```js
var jsonObject = questionnaireEngine.serializeQuestionnaireResponse();
```

## License
The QuestionnaireEngine source code is licensed under the MIT license.